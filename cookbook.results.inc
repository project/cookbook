<?php


/**
 * @file
 *  cookbook.results.inc php file
 *  Contains functions related to displaying results for the Drupal module
 *  cookbook.
 */

/**
 * Function to determine if we're going to order by the given field and
 * in which direction.
 *
 * @param string $order
 *  Given field and direction to order in.
 * @param string $field
 *  Field to check for.
 *
 * @return array
 *  First child contains html(classes), second direction.
 */
function cookbook_order($order, $field) {
  $orders = explode('-', $order);
  if ($orders[0] == $field || $field == 'title' && !in_array($orders[0], array('time', 'difficulty'))) {
    $orders[0] = ' class="selected';
    if ($orders[1] == 'DESC') {
      $orders[0] .= ' reverse';
      $orders[1] = 'ASC';
    }
    else {
      $orders[1] = 'DESC';
    }
    $orders[0] .= '"';
  }
  else {
    $orders[0] = '';
    $orders[1] = 'ASC';
  }
  return $orders;
}

/**
 * A recursive function that sorts the different category levels into arrays
 *
 * @param array $mixed_categories
 *  A list of all the categories
 * @param int $selected_category
 *  The selected category to look for.
 * @param int $parent
 *  Parent for these categories.
 * @param int $level
 *  The current level.
 *
 * @return array
 *  The sorted categories.
 */
function cookbook_sort_categories($mixed_categories, $selected_category, $parent = 0, $level = 0, $search = false) {
  $categories = array();
  foreach ($mixed_categories as $category) {
    if ($category->parents[0] == $parent) {
      if ($category->tid == $selected_category || $selected_category == 0) {
        $search = true;
      }
      $children = cookbook_sort_categories($mixed_categories, $selected_category, $category->tid, $level + 1, $search);

      // Merge children with other children
      foreach ($children as $index => $child) {
        if (is_array($categories[$index])) {
          foreach ($child as $tid => $term) {
            if ($index == 'search') {
              $categories['search'][] = $term;
            }
            else {
              $categories[$index][$tid] = $term;
            }
          }
        }
        else {
          $categories[$index] = $child;
        }
      }

      // Add category
      $categories[$level][$category->tid] = array(
        'name' => $category->name,
        'parent' => $parent,
        'children' => count($children[$level + 1]),
      );

      // Add active categories
      if ($category->tid == $selected_category || count($children['active']) > 0) {
        $categories['active'][] = $category->tid;
      }

      // Add search categories
      if ($search) {
        $categories['search'][] = $category->tid;
      }
      if ($category->tid == $selected_category) {
        $search = false;
      }
    }
  }
  return $categories;
}

/**
 * A recursive function that finds the tid for all sub categories of the given
 * category.
 *
 * @param int $parent
 *  Parent category tid.
 * @param array $mixed
 * _categories The list of categories to look in.
 *
 * @return array
 *  A list with the tid of every sub category.
 */
function cookbook_get_sub_categories($parent, $categories) {
  $sub_categories = array();
  foreach ($categories as $category) {
    if ($category->parents[0] == $parent) {
      $children = cookbook_get_sub_categories($category->tid, $categories);
      $sub_categories = array_merge($sub_categories, $children);
      $sub_categories[] = $category->tid;
    }
  }
  return $sub_categories;
}

/**
 * Function to find recipes.
 *
 * @param array $search_categories
 *  Categories to search in.
 * @param array $search_words
 *  Words to filter title by.
 * @param string $order
 *  Field and direction to order in.
 * @param string $page
 *  Page to limit results to.
 *
 * @return array
 *  0 to 6 recipes
 */
function cookbook_get_recipes($search_categories, $search_words, $order, $page) {
  // TODO: Split up into a data provider and a viewer.

  // Query arguments
  $query_args = array();
  
  // Query
  $query = "SELECT DISTINCT count(*) as occ, a.vid, a.nid, a.title, a.body,
    time.tid as time_tid, time.name as time, diff.tid as diff_tid,
    diff.name as diff FROM (
      SELECT n.nid, n.vid, n.title, nr.body FROM {node} n JOIN {node_revisions} nr ON n.type = 'recipe' AND n.status = 1";

  if (module_exists('i18n')) {
    $query .= " AND n.language IN ('%s', '')";
    $query_args[] = i18n_get_lang();
  }

  $query .= " AND n.vid = nr.vid";

  // Add search keywords
  if (isset($search_words)) {
    foreach ($search_words as $keyword) {
      // Search title
      $query .= " UNION ALL SELECT n.nid, n.vid, n.title, nr.body FROM {node} n JOIN {node_revisions} nr ON n.type = 'recipe' AND n.status = 1";
      if (module_exists('i18n')) {
        $query .= " AND n.language IN ('%s', '')";
        $query_args[] = i18n_get_lang();
      }
      $query .= " AND n.title LIKE '%%%s%%' AND n.vid = nr.vid";
      $query_args[] = $keyword;
      // Search taxonomy
      $query .= " UNION ALL SELECT n.nid, n.vid, n.title, nr.body FROM {node} n JOIN {term_node} tn ON n.type = 'recipe' AND n.status = 1";
      if (module_exists('i18n')) {
        $query .= " AND n.language IN ('%s', '')";
        $query_args[] = i18n_get_lang();
      }
      $query .= " AND n.vid = tn.vid JOIN {term_data} td ON td.vid = %d AND td.name LIKE '%%%s%%' AND td.tid = tn.tid JOIN {node_revisions} nr ON n.vid = nr.vid";
      $query_args[] = variable_get('cookbook_search', NULL);
      $query_args[] = $keyword;
    }
  }

  // Continue query
  $query .= ') a';

  // Filter on categories
  if (is_array($search_categories)) {
    $query .= ' JOIN (SELECT DISTINCT vid FROM {term_node} WHERE tid IN (%s)) tn ON a.vid = tn.vid';
    $query_args[] = implode(', ', $search_categories);
  }

  // Continue query
  $query .= ' JOIN (
          SELECT tn.vid, tn.tid, td.name FROM {term_node} tn
          JOIN {term_data} td ON td.vid = %d AND td.tid = tn.tid
         ) time ON time.vid = a.vid
    JOIN (
          SELECT tn.vid, tn.tid, td.name FROM {term_node} tn
          JOIN {term_data} td ON td.vid = %d AND td.tid = tn.tid
         ) diff ON diff.vid = a.vid
    JOIN {node} n ON a.vid = n.vid';

  // Add more arguments
  $query_args[] = variable_get('cookbook_time_consumption', NULL);
  $query_args[] = variable_get('cookbook_difficulty', NULL);

  $query .= " GROUP BY a.vid";

  if (isset($search_words)) {
    // Only display relevant recipes
    $query .= ' HAVING occ > 1';
  }

  $query .= " ORDER BY occ DESC, %s %s";

  // Add ordering
  $orders = explode('-', $order);
  if ($orders[0] == 'time') {
    $query_args[] = 'time.tid';
  }
  else if ($orders[0] == 'difficulty') {
    $query_args[] = 'diff.tid';
  }
  else {
    $query_args[] = 'a.title';
  }

  if ($orders[1] == 'DESC') {
    $query_args[] = 'DESC';
  }
  else {
    $query_args[] = 'ASC';
  }

  $result = db_query_range($query, $query_args, $page * 6, 6);

  while ($recipe = db_fetch_object($result)) {
    // Load first image for the recipe
    $recipe->image = db_fetch_array(
        db_query(
          "SELECT source FROM {cookbook_recipe_images}
            WHERE recipe_vid = %d ORDER BY id LIMIT 1",
          $recipe->vid
        )
    );

    if (is_array($recipe->image)) {
      $recipe->image = end($recipe->image);
    }
    else {
      // Use default image
      $recipe->image = base_path() . drupal_get_path('module', 'cookbook') . '/theme/images/default-recipe.png';
    }
    if (is_numeric($recipe->image)) {
      $recipe->image = file_create_url(db_result(db_query("SELECT f.filepath FROM {files} f JOIN {image} i ON i.nid = %d AND i.image_size = '%s' AND f.fid = i.fid", $recipe->image, variable_get('cookbook_image_node_thumb_size', 'thumbnail'))));
    }

    // Translate taxonomy
    $vocabulary_id = variable_get('cookbook_categories', NULL);
    $translate = variable_get('i18ntaxonomy_vocabulary', array($vocabulary_id => 0));
    if ($translate[$vocabulary_id] == 1) {
      $recipe->time = i18nstrings("taxonomy:term:$recipe->time_tid:name", $recipe->time);
      $recipe->diff = i18nstrings("taxonomy:term:$recipe->diff_tid:name", $recipe->diff);
    }

    $recipes[] = $recipe;
  }
  return $recipes;
}

/**
 * Function to generate the cookbooks results page.
 *
 * @param int $category
 *  Selected category id.
 * @param string $search
 *  Keywords to filter title on.
 * @param string $order
 *  Field and direction to order recipes by.
 * @param int $page
 *  Which page of results to display.
 *
 * @return string
 *  Themed output.
 */
function cookbook_results($category, $search, $order, $page) {
  // Add javascript
  cookbook_add_js();
  drupal_add_js(drupal_get_path('module', 'cookbook') . '/theme/javascripts/cookbook-results.min.js');

  // Get categories
  if (module_exists('i18ntaxonomy')) {
    $mixed_categories = cookbook_get_localized_tree(variable_get('cookbook_categories', NULL));
  }
  else {
    $mixed_categories = taxonomy_get_tree(variable_get('cookbook_categories', NULL));
  }

  // Sort all mixed categories into $categories to improve runtime
  $categories = cookbook_sort_categories($mixed_categories, $category);
  if (is_array($categories['active'])) {
    $categories['active'] = array_reverse($categories['active']);
  }

  // Split up keywords to search for
  if ($search != '0') {
    $search_words = explode(' ', $search);
  }

  if ($category == 0) {
    $categories['search'] = 0;
  }

  // Get number of hits
  $hits = cookbook_get_hits($categories['search'], $search_words);

  // Find recipes
  $recipes = cookbook_get_recipes($categories['search'], $search_words, $order, $page);

  // Print theme
  return theme('cookbook_results', $categories, $recipes, $hits, $category, $search, $order, $page);
}

/**
 * Function to generate results for an XMLHttpRequest.
 *
 * @param int $category
 *  Selected category id.
 * @param string $search
 *  Keywords to filter title on.
 * @param string $order
 *  Field and direction to order recipes by.
 * @param int $page
 *  Which page of results to display.
 *
 * @return string
 *  Themed output.
 */
function cookbook_results_xhr($category, $search, $order, $page) {
  // Get all categories
  $mixed_categories = taxonomy_get_tree(variable_get('cookbook_categories', NULL));

  if ($category == 0) {
    $search_categories = 0;
  }
  else {
    $search_categories = cookbook_get_sub_categories($category, $mixed_categories);
    $search_categories[] = $category;
  }

  // Split up keywords to search for
  if ($search != '0') {
    $search_words = explode(' ', $search);
  }

  // Get number of hits
  $hits = cookbook_get_hits($search_categories, $search_words);

  // Find recipes
  $recipes = cookbook_get_recipes($search_categories, $search_words, $order, $page);

  // Print themed results
  print theme('cookbook_results_xhr', $recipes, $hits, $category, $search, $order, $page);
}