<?php


/**
 * @file
 *  cookbook.admin.inc php file
 *  Contains admin functions for the Drupal module cookbook.
 */

/**
 * Implementation of hook_admin
 */
function cookbook_admin() {
  $form['cookbook_logo'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo'),
    '#default_value' => variable_get('cookbook_logo', ''),
    '#description' => t('URL for the logo to display at the top of the cookbook.'),
  );
  $form['cookbook_only_node_images'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only image nodes'),
    '#default_value' => variable_get('cookbook_only_node_images', 0),
    '#description' => t("Option to only allow images in recipes that are image nodes."),
  );
  $form['cookbook_use_ingredient_nodes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use ingredient nodes'),
    '#default_value' => variable_get('cookbook_use_ingredient_nodes', 0),
    '#description' => t("Option to enable the use of ingredient nodes with nutrition data in recipes."),
  );
  $form['cookbook_nutrition_data_reference'] = array(
    '#type' => 'textfield',
    '#title' => t('Nutrition data reference'),
    '#default_value' => variable_get('cookbook_nutrition_data_reference', ''),
    '#description' => t('Refrence for the nutrition data source.'),
  );

  // VOCABULARIES
  $form['cookbook_vocabularies'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vocabularies'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Get list of vocabularies
  $vocabularies = taxonomy_get_vocabularies();
  $recipe_vocabularies = array();
  $all_vocabularies = array();
  foreach ($vocabularies as $vocabulary) {
    if (in_array('recipe', $vocabulary->nodes)) {
      $recipe_vocabularies[$vocabulary->vid] = $vocabulary->name;
    }
    $all_vocabularies[$vocabulary->vid] = $vocabulary->name;
  }
  $form['cookbook_vocabularies']['cookbook_dictionary'] = array(
    '#type' => 'select',
    '#title' => t('Dictionary'),
    '#default_value' => variable_get('cookbook_dictionary', NULL),
    '#options' => $all_vocabularies,
    '#description' => t("The vocabulary to use as a dictionary."),
  );
  $form['cookbook_vocabularies']['cookbook_categories'] = array(
    '#type' => 'select',
    '#title' => t('Categories'),
    '#default_value' => variable_get('cookbook_categories', NULL),
    '#options' => $recipe_vocabularies,
    '#description' => t("The vocabulary to use for organizing recipes."),
  );
  $form['cookbook_vocabularies']['cookbook_time_consumption'] = array(
    '#type' => 'select',
    '#title' => t('Time consumption'),
    '#default_value' => variable_get('cookbook_time_consumption', NULL),
    '#options' => $recipe_vocabularies,
    '#description' => t("The vocabulary to use for recipes time consumption."),
  );
  $form['cookbook_vocabularies']['cookbook_difficulty'] = array(
    '#type' => 'select',
    '#title' => t('Difficulty'),
    '#default_value' => variable_get('cookbook_difficulty', NULL),
    '#options' => $recipe_vocabularies,
    '#description' => t("The vocabulary to use for recipes difficulty."),
  );
  $form['cookbook_vocabularies']['cookbook_search'] = array(
    '#type' => 'select',
    '#title' => t('Search'),
    '#default_value' => variable_get('cookbook_search', NULL),
    '#options' => $recipe_vocabularies,
    '#description' => t("The vocabulary to use when searching for recipes."),
  );
  $form['cookbook_vocabularies']['cookbook_commodities'] = array(
    '#type' => 'select',
    '#title' => t('Commodities'),
    '#default_value' => variable_get('cookbook_commodities', NULL),
    '#options' => $all_vocabularies,
    '#description' => t("The vocabulary to use for organizing commodities in the kitchen drawer."),
  );

  // BLOCKS
  $form['cookbook_blocks'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blocks'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $blocks = array('' => '-');
  foreach (_block_rehash () as $block) {
    $blocks[$block['module'] . '-' . $block['delta']] = $block['info'];
  }
  $form['cookbook_blocks']['cookbook_top_block'] = array(
    '#type' => 'select',
    '#title' => t('Custom top block'),
    '#default_value' => variable_get('cookbook_top_block', NULL),
    '#options' => $blocks,
    '#description' => t("A custom block to display on the top of the page. E.g. a language switcher."),
  );
  $form['cookbook_blocks']['cookbook_block'] = array(
    '#type' => 'select',
    '#title' => t('Custom recipe block'),
    '#default_value' => variable_get('cookbook_block', NULL),
    '#options' => $blocks,
    '#description' => t("A custom block to display on the recipe page. E.g. a readspeaker."),
  );

  if (module_exists('image')) {
    // IMAGE SIZES
    $form['cookbook_image_sizes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Image sizes'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $image_sizes = image_get_sizes();
    foreach ($image_sizes as $key => $image_size) {
      $image_sizes[$key] = $image_size['label'];
    }
    $form['cookbook_image_sizes']['cookbook_image_node_recipe_size'] = array(
      '#type' => 'select',
      '#title' => t('Image node size on recipe page'),
      '#default_value' => variable_get('cookbook_image_node_recipe_size', '_original'),
      '#options' => $image_sizes,
      '#description' => t("Size of the image nodes to use on the recipe page. Recommended: 654x250"),
    );
    $form['cookbook_image_sizes']['cookbook_image_node_thumb_size'] = array(
      '#type' => 'select',
      '#title' => t('Image node size on thumbnails'),
      '#default_value' => variable_get('cookbook_image_node_thumb_size', 'thumbnail'),
      '#options' => $image_sizes,
      '#description' => t("Size of the image nodes to use as thumbnails. Recommended: 116x87"),
    );
  }

  $form['cookbook_import_ingredients'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import ingredients'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  $form['cookbook_import_ingredients']['cookbook_import'] = array(
    '#type' => 'file',
    '#title' => t('Import ingredients'),
    '#description' => t('Choose a csv file to import ingredients from. Each line must be in the following format: "Name";"unit";grams;"unit";grams;"unit";grams;energy;protein;fat;carbohydrate;saturated+trans fatty acids;cis-monounsaturated;cis-polyunsaturated fatty acids;cholesterol;starch;mono+disaccharides;dietary fiber;added sugar;alcohol;calcium;iron;sodium;potassium;magnesium;zinc;selenium;phosphorus;vitamin A;vitamin D;vitamin E;thiamine (vitamin B1);riboflavin (vitamin B2);niacin equivalents;vitamin B6;folate;vitamin C;water;trans fatty acids e.g. "Cheese";"slice";9;"portion";25;"gallon";45.89;3.3;1.5;1;0.4;0;7;4.7;0;4.7;0;0;0;100;0;40;157;13;0.4;1;88;14;0;0;0.05;0.15;0.8;0.04;5;0;90;0.1'),
  );

  $langs = array('0' => 'Language Neutral');
  foreach (language_list () as $lang) {
    $langs[$lang->language] = $lang->name;
  }
  $form['cookbook_import_ingredients']['cookbook_import_language'] = array(
    '#type' => 'select',
    '#title' => '',
    '#default_value' => 0,
    '#options' => $langs,
    '#description' => t("Language for the ingredients that's going to be imported."),
  );
  $form['#submit'][] = 'cookbook_admin_submit';
  return system_settings_form($form);
}

/**
 * Implementation of cookbook_admin_validate
 */
function cookbook_admin_validate($form, &$form_state) {
  global $user;

  if ($form_state['values']['cookbook_logo'] != '') {
    if (!valid_url($form_state['values']['cookbook_logo'])) {
      form_set_error('cookbook_logo', t('Please supply a valid url, or leave blank'));
    }
  }

  // Import ingredients from csv file
  if (file_exists($_FILES['files']['tmp_name']['cookbook_import'])) {
    // Get lines
    $form_state['storage']['cookbook_ingredients_lines'] = file($_FILES['files']['tmp_name']['cookbook_import']);
  }
  else {
    unset($form_state['storage']['cookbook_ingredients_lines']);
  }
}

/**
 * Submit the cookbook_admin form
 */
function cookbook_admin_submit($form, &$form_state) {
  if (isset($form_state['storage']['cookbook_ingredients_lines'])) {
    $lines = $form_state['storage']['cookbook_ingredients_lines'];

    // Counter
    $inserts = 0;

    // Go through each line
    foreach ($lines as $line) {
      // Make sure we have 39 fields
      $ingredient = explode(';', $line);
      if (count($ingredient) == 39) {
        for ($i = 0; $i < 39; $i++) {
          if ($ingredient[$i][0] == '"') {
            $ingredient[$i] = substr($ingredient[$i], 1, -1);
          }
        }

        // Create ingredient node for line
        $node = new stdClass();
        $node->title = trim($ingredient[0]);
        $node->type = 'ingredient';
        $node->status = 1;
        if ($form_state['values']['cookbook_import_language'] != '0') {
          $node->language = $form_state['values']['cookbook_import_language'];
        }
        for ($i = 1; $i < 6; $i+=2) {
          if ($ingredient[$i] != '') {
            $node->units .= '@0;' . $ingredient[$i] . ';' . $ingredient[$i + 1];
          }
        }

        // Let's make missing values into -1
        for ($i = 7; $i < 39; $i++) {
          if ($ingredient[$i] == '' || $ingredient[$i] == 'M') {
            $ingredient[$i] = -1;
          }
        }

        $n = 7;
        foreach (cookbook_get_nutrients () as $nutrient => $attrs) {
          $node->$nutrient = is_numeric($ingredient[$n]) ? $ingredient[$n] : -1;
          $n++;
        }

        $node->auto_created = TRUE;
        $node->uid = $user->uid;
        node_save($node);

        $inserts++;
      }
    }
    drupal_set_message(t("Imported %num ingredients.", array('%num' => $inserts)));
  }
}