<?php


/**
 * @file
 *  cookbook.recipe.inc php file
 *  Contains functions related to displaying a recipe for the Drupal module
 *  cookbook.
 */

/**
 * Function to generate the cookbooks recipe page.
 *
 * @param int $category
 *  Selected category id.
 * @param string $search
 *  Keywords to filter title on.
 * @param string $order
 *  Field and direction to order recipes by.
 * @param int $page
 *  Which page of results to display.
 * @param int $nid
 *  The id of the recipe to display.
 *
 * @return string
 *  Themed output.
 */
function cookbook_recipe($category, $search, $order, $page, $nid) {
  // TODO: Split this function up. It is 140 lines long...

  cookbook_add_js();
  drupal_add_js(drupal_get_path('module', 'cookbook') . '/theme/javascripts/cookbook-recipe.min.js');

  $recipe = node_load($nid);

  // Display front page if the recipe doesn't exist.
  if (!$recipe) {
    drupal_set_message(t('Selected recipe does not exist!'), 'error');
    return cookbook_front();
  }

  // Images
  if (isset($recipe->images)) {
    foreach ($recipe->images as $image) {
      if (is_numeric($image->source)) {
        $image->source = file_create_url(db_result(db_query("SELECT f.filepath FROM {files} f JOIN {image} i ON i.nid = %d AND i.image_size = '%s' AND f.fid = i.fid", $image->source, variable_get('cookbook_image_node_recipe_size', '_original'))));
      }
    }
  }
  else {
    // Use default image
    $default_image = new stdClass();
    $default_image->id = 0;
    $default_image->source = base_path() . drupal_get_path('module', 'cookbook') . '/theme/images/default-recipe-big.png';
    $recipe->images[] = $default_image;
  }

  // Ingredients
  if (variable_get('cookbook_use_ingredient_nodes', 0) == 1) {
    $recipe->ingredients = array();

    $result = db_query(
        "SELECT cri.ingredient_nid, cri.unit_id, n.title, cri.amount, cri.display,
        ci.energy, ci.protein, ci.fat, ci.carbohydrate,
        ci.saturedtransfattyacids, ci.cismonounsaturated,
        ci.cispolyunsaturatedfattyacids, ci.cholesterol, ci.starch,
        ci.monodisaccharides, ci.dietaryfiber, ci.addedsugar, ci.alcohol,
        ci.calcium, ci.iron, ci.sodium, ci.potassium, ci.magnesium, ci.zinc,
        ci.selenium, ci.phosphorus, ci.vitamina, ci.vitamind, ci.vitamine,
        ci.thiamine, ci.riboflavin, ci.niacinequivalents, ci.vitaminb6,
        ci.folate, ci.vitaminc, ci.water, ci.transfattyacids, cu.name, cu.grams
      FROM {cookbook_recipe_ingredients} cri
      JOIN {node} n ON cri.ingredient_nid = n.nid
      JOIN {cookbook_ingredients} ci ON ci.vid = n.vid
      LEFT JOIN {cookbook_units} cu ON cri.unit_id = cu.id
      WHERE cri.recipe_vid = %d
      ORDER BY cri.id",
        $recipe->vid
    );
    while ($ingredient = db_fetch_array($result)) {
      if ($ingredient['name'] == NULL) {
        $ingredient['name'] = 'g';
        $multiplier = $ingredient['amount'] * 0.01;
      }
      else {
        $multiplier = $ingredient['amount'] * $ingredient['grams'] * 0.01;
      }
      $recipe->ingredients[] = '<span class="ingredient-amount">' . round($ingredient['amount'] * 4, 3) . '</span><span class="hidden">' . $ingredient['amount'] . '</span> ' . check_plain($ingredient['name'] . ' ' . strtolower($ingredient['display']));

      foreach (cookbook_get_nutrients () as $nutrient => $attr) {
        if ($ingredient[$nutrient] == -1) {
          $recipe->$nutrient = -1;
        }
        else if ($recipe->$nutrient != -1) {
          $recipe->$nutrient += $ingredient[$nutrient] * $multiplier;
        }
      }
    }

    foreach (cookbook_get_nutrients () as $nutrient => $attr) {
      if ($recipe->$nutrient != -1) {
        if ($nutrient == 'energy') {
          $recipe->$nutrient = round($recipe->$nutrient, 0);
        }
        else {
          $recipe->$nutrient = round($recipe->$nutrient, 2);
        }
      }
    }
  }
  else {
    $recipe->ingredients = explode('@', $recipe->ingredients);
    foreach ($recipe->ingredients as $key => $ingredient) {
      $words = explode(' ', $ingredient);
      if (is_numeric($words[0])) {
        $recipe->ingredients[$key] = '<span class="ingredient-amount">' . $words[0] . '</span><span class="hidden">' . $words[0] / 4 . '</span> ';
        array_shift($words);
        $recipe->ingredients[$key] .= check_plain(implode(' ', $words));
      }
    }
  }

  $recipe->steps = check_plain($recipe->steps);

  // Get dictionary terms and synonyms and highlight them in the steps.
  $query = "SELECT ts.name as synonym, td.name, td.description FROM {term_synonym} ts RIGHT JOIN {term_data} td ON ts.tid = td.tid WHERE td.vid = %d";
  $query_args = array(variable_get('cookbook_dictionary', NULL));

  if (module_exists('i18ntaxonomy')) {
    $query .= " AND td.language = '%s'";
    $query_args[] = i18n_get_lang();
  }

  $checked = array();
  $result = db_query($query, $query_args);
  while ($term = db_fetch_array($result)) {
    if (!$checked[$term['name']]) {
      $recipe->steps = preg_replace("'\b(" . $term['name'] . ")\b'i", "<span class=\"term\">$1</span><span class=\"term-description\">" . $term['description'] . '</span><img src="' . base_path() . drupal_get_path('module', 'cookbook') . '/theme/images/term-tip-pointer.png" class="term-pointer" alt=""/>', $recipe->steps);
      $checked[$term['name']] = TRUE;
    }
    if ($term['synonym'] != NULL && !$checked[$term['synonym']]) {
      $recipe->steps = preg_replace("'\b(" . $term['synonym'] . ")\b'i", "<span class=\"term\">$1</span><span class=\"term-description\">" . $term['description'] . '</span><img src="' . base_path() . drupal_get_path('module', 'cookbook') . '/theme/images/term-tip-pointer.png" class="term-pointer" alt=""/>', $recipe->steps);
      $checked[$term['synonym']] = TRUE;
    }
  }

  // Split up steps
  $steps = explode('@', $recipe->steps);
  $recipe->steps = array();
  for ($i = 1; $i < count($steps); $i++) {
    $recipe->steps[] = $steps[$i];
  }

  // Taxonomy
  foreach ($recipe->taxonomy as $taxonomy) {
    if ($taxonomy->vid == variable_get('cookbook_time_consumption', NULL)) {
      $recipe->time = $taxonomy->name;
      $recipe->time_tid = $taxonomy->tid;
    }
    else if ($taxonomy->vid == variable_get('cookbook_difficulty', NULL)) {
      $recipe->diff = $taxonomy->name;
      $recipe->diff_tid = $taxonomy->tid;
    }
  }

  // Translate taxonomy
  $vocabulary_id = variable_get('cookbook_categories', NULL);
  $translate = variable_get('i18ntaxonomy_vocabulary', array($vocabulary_id => 0));
  if ($translate[$vocabulary_id] == 1) {
    $recipe->time = i18nstrings("taxonomy:term:$recipe->time_tid:name", $recipe->time);
    $recipe->diff = i18nstrings("taxonomy:term:$recipe->diff_tid:name", $recipe->diff);
  }

  // Fetch custom block
  $block = variable_get('cookbook_block', NULL);
  if (isset($block)) {
    $block_ids = explode('-', $block);
    $block = module_invoke($block_ids[0], 'block', 'view', $block_ids[1]);
  }
  return theme('cookbook_recipe', $recipe, $category, $search, $order, $page, $block);
}