
/**
 * Init the cookbook
 */
$(document).ready(function() {
  // Display all drupal messages to the user
  $('.messages').each(Cookbook.showMessage);

  // Fading effect on links.
  $('.fading').textColorFade();
  $('.reverse-fading').textColorFade({'color': '#550700'});
  $('#top-block a').textColorFade();

  // Init the kitchen drawer
  Cookbook.drawerLoader = $('#drawer-content-content').html();
  $('#drawer-front').click(Cookbook.openDrawer);
  $('#drawer-inside').scrollTop(400);
  $('.drawer-menu').click(Cookbook.selectDrawerMenu);
  $('#drawer-content-top p').click(function() {
    $('#drawer-inside').stop().animate({
      'scrollTop': '220px'
    }, 500);
    if ($.browser.msie) {
      $('#drawer-content').animate({
        'zoom': '0px'
      }, 500);
    }
  });
  if ($.browser.msie) {
    // Jump start IE7
    $('#drawer-inside').css('zoom', '0px');
  }

  // Search submit
  $('#search').submit(function() {
    var url = $('#search').attr('action').split('/');
    url[url.length - 2] = Cookbook.getCategory();
    url[url.length - 1] = Cookbook.getOrder();
    $('#search').attr('action', url.join('/'));
  });
});

/**
 * jQuery Text Color Fading
 * Based on dwFadingLinks by David Walsh
 */
jQuery.fn.textColorFade = function(settings) {
  settings = jQuery.extend({
    color: '#990000',
    duration: 500
  }, settings);
  return this.each(function() {
    var originalColor = $(this).css('color');
    $(this).mouseover(function() {
      if (!$(this).parent().hasClass('active'))
        $(this).stop().animate({
          color: settings.color
        }, settings.duration);
    });
    $(this).mouseout(function() {
      if (!$(this).parent().hasClass('active'))
        $(this).stop().animate({
          color: originalColor
        }, settings.duration);
    });
  });
}

/**
 * Container for all cookbook functions.
 */
var Cookbook = {

  /**
   * Get the current category.
   *
   * @return
   *   Category identifier
   */
  getCategory: function() {
    var category;
    if ($('#categories td.selected').get()[0] == undefined)
      category = 0;
    else {
      if ($('#sub-categories li.active').hasClass('show-all')) {
        category = $('#categories td.selected').attr('id').split('-')[1];
      } else {
        if ($('#sub-sub-categories td.active').hasClass('show-all'))
          category = $('#sub-categories li.active').attr('id').split('-')[1];
        else {
          if ($('#sub-sub-sub-categories td.active').hasClass('show-all'))
            category = $('#sub-sub-categories td.active').attr('id').split('-')[1];
          else {
            category = $('#sub-sub-sub-categories td.active').attr('id').split('-')[1];
          }
        }
      }
    }
    return category;
  },

  /**
   * Get the current sorting order.
   *
   * @return
   *   field-(ASC|DESC)
   */
  getOrder: function() {
    var order;
    if ($('#results-order ul li.selected')[0] == undefined)
      order = 0;
    else {
      order = $('#results-order ul li.selected').attr('id').split('-')[1];
      if ($('#results-order ul li.selected').hasClass('reverse'))
        order += '-DESC'
      else
        order += '-ASC'
    }
    return order;
  },

  /**
   * Show drupal message.
   *
   * @param i
   *   Counter.
   *
   * @param message
   *   The message.
   */
  showMessage: function(i, message) {
    // Fade in each message every 1/2 second
    setTimeout(function() {
      // Position the message
      $(message).css({
        'top': i * 70 + 130 + 'px',
        'right': '20px'
      }).fadeIn('slow');
      // Fade out after 5 seconds
      setTimeout(function() {
        $(message).fadeOut('slow');
      }, 5000);
    }, i * 500);
  },

  /**
   * Open the kitchen drawer.
   */
  openDrawer: function() {
    $('#drawer-inside').stop().animate({
      'height': '220px',
      'scrollTop': '220px'
    }, 500);
    $(this).unbind('click').click(Cookbook.closeDrawer).children('h3').stop().animate({
      'height': '0px',
      'paddingTop': '10px'
    }, 500);
  },

  /**
   * Close the kitchen drawer.
   */
  closeDrawer: function() {
    $('#drawer-inside').stop().animate({
      'height': '0px',
      'scrollTop':  $('#drawer-inside')[0].scrollTop + 220 + 'px'
    }, 500, function() {
      // Make sure drawer is ready to be opened
      $('#drawer-inside')[0].scrollTop = 440;
    });
    $(this).unbind('click').click(Cookbook.openDrawer).children('h3').stop().animate({
      'height': '18px',
      'paddingTop': '4px'
    }, 500);
  },

  /**
   * Select an item from the kitchen drawer's menu.
   */
  selectDrawerMenu: function() {
    $('#drawer-content-top h3').html($(this).children('h3').html());
    if ($(this).attr('id') != 'measurement') {
      $('#drawer-content-content').html('').append('<div id="drawer-search"><input type="text" value="' + Drupal.t('Search') + '"/></div><div id="drawer-titles-wrapper"><ul id="drawer-titles"></ul></div><ul id="drawer-descriptions"></ul>');
      $('#drawer-search input').keyup(Cookbook.filterTitles).focus(Cookbook.searchFocus);
    }
    Cookbook.loadDrawerCategories($(this).attr('id'));

    $('#drawer-inside').animate({
      'scrollTop': '0px'
    }, 500);
    if ($.browser.msie) {
      $('#drawer-content').animate({
        'zoom': '220px'
      }, 500)
    }
  },

  /**
   * Load categories for an kitchen section.
   *
   * @param section
   *   The section to load categories for.
   */
  loadDrawerCategories: function(section) {
    if (section == 'dictionary') {
      $.getJSON(Drupal.settings['cookbook']['url'] + '/xhr/dictionary/letters', Cookbook.displayLetters);
    }
    else if (section == 'measurement') {
      $.getJSON(Drupal.settings['cookbook']['url'] + '/xhr/converter', Cookbook.displayConverterCategories);
    }
    else if (section == 'commodities') {
      $.getJSON(Drupal.settings['cookbook']['url'] + '/xhr/commodities/categories', Cookbook.displayCommoditiesCategories);
    }
  },

  /**
   * Display letters for the kitchen drawer dictionary.
   *
   * @param data
   *   Data to display.
   */
  displayLetters: function(data) {
    $('#drawer-content-top ul li').remove();
    $('<li><span>All</span></li>').appendTo('#drawer-content-top ul').click(Cookbook.clickLetter).click();
    $.each(data, function(i, letter) {
      $('<li><span>' + letter + '</span></li>').click(Cookbook.clickLetter).appendTo('#drawer-content-top ul');
    })
  },

  /**
   * Select a letter in the kitchen drawer dictionary.
   */
  clickLetter: function() {
    var letter = $(this).children().html();
    if (letter.length > 1) {
      letter = '@'
    }
    Cookbook.loadWords(letter);
    $('#drawer-content-top ul li').removeClass('selected');
    $(this).addClass('selected');
  },

  /**
   * Load words for the kitchen drawer dictionary.
   *
   * @param letter First letter in the words.
   */
  loadWords: function(letter) {
    $('#drawer-titles li').remove();
    $('#drawer-descriptions li').remove();
    $('#drawer-titles').append('<li>' + Cookbook.drawerLoader + '</li>');
    $.getJSON(Drupal.settings['cookbook']['url'] + '/xhr/dictionary/letter/' + encodeURI(letter), Cookbook.displayWords);
  },

  /**
   * Display words for the kitchen drawer dictionary.
   *
   * @param data
   *   Data for the words.
   */
  displayWords: function(data) {
    $('#drawer-titles li').remove();
    $.each(data, function(i, word) {
      $('<li>' + word['name'] + '</li>').appendTo('#drawer-titles').click(function() {
        $('#drawer-descriptions').animate({
          'scrollTop': $('#description-' + word['tid'])[0].offsetTop
        }, 500);
      });
      $('<li id="description-' + word['tid'] + '"><h3>' + word['name'] + '</h3><p>' + word['description'] + '</p></li>').appendTo('#drawer-descriptions');
    });
    $('#drawer-titles').jScrollPane({
      'dragMinHeight': 15
    });
  },

  /**
   * Display categories for the kitchen drawer converter.
   *
   * @param data
   *   Data for the categories.
   */
  displayConverterCategories: function(data) {
    $('#drawer-content-top ul li').remove();
    Cookbook.converterCategories = data;
    $('#drawer-content-content').html('').append('<div id="converter-tip"><span id="converter-tip-1">1. ' + Drupal.t('Type in the value you wish to convert.') + '</span><br/><span id="converter-tip-2">2. ' + Drupal.t('Select the unit of measurement for your value.') + '</span><br/><span id="converter-tip-3">3. ' + Drupal.t('Select the unit of measurement you would like to convert your value to.') + '</span></div><div id="converter"><span id="convert-from"><input type="text" class="converter-tip-1"/><select class="converter-tip-2"></select></span> = <span id="convert-to"><input type="text" readonly/><select class="converter-tip-3"></select></span></div>');
    $('#convert-from input').focus(Cookbook.highlightConverter).focus().keyup(Cookbook.convert).blur(Cookbook.removeConverterHighlight).next().focus(Cookbook.highlightConverter).change(Cookbook.convert).blur(Cookbook.removeConverterHighlight);
    $('#convert-to select').focus(Cookbook.highlightConverter).change(Cookbook.convert).blur(Cookbook.removeConverterHighlight).prev().focus(function() {
      $(this).next().focus();
    });
    var counter = 0;
    $.each(data, function(i, category) {
      var li = $('<li><span>' + i + '</span></li>').css('text-transform', 'capitalize').click(Cookbook.clickConverterCategory).appendTo('#drawer-content-top ul');
      if (counter == 0)
        li.click();
      counter++;
    });
  },

  /**
   * Highlight a field in the converter.
   */
  highlightConverter: function() {
    $('#' + $(this).attr('class')).css('color', '#dcb85f');
    $(this).css('outline', '1px solid #dcb85f');
  },

   /**
   * Removes the highlight from a field in the converter.
   */
  removeConverterHighlight: function() {
    $('#converter-tip span').css('color', '#a07b24');
    $(this).css('outline', 'none');
  },

  /**
   * Select a category in the kitchen drawer converter.
   */
  clickConverterCategory: function() {
    $('#drawer-content-top li.selected').removeClass('selected');
    var selected = $(this).addClass('selected').children().html();
    $('#convert-from option').remove();
    $('#convert-to option').remove();
    $.each(Cookbook.converterCategories[selected], function(i, value) {
      $('<option value="' + value['ratio'] + '">' + value['name'] + '</option>').appendTo('#convert-from select').clone().appendTo('#convert-to select');
    });
    $('#convert-from input').focus();
    Cookbook.convert();
  },

  /**
   * Convert the input in the kitchen drawer converter.
   */
  convert: function() {
    var val;
    if ($('#convert-from option:selected').html() == 'celsius') {
      if ($('#convert-to option:selected').html() == 'fahrenheit') {
        val = (parseFloat($('#convert-from input').val(), 10) * (9 / 5)) + 32;
      } else {
        val = $('#convert-from input').val();
      }
    }
    else if ($('#convert-from option:selected').html() == 'fahrenheit') {
      if ($('#convert-to option:selected').html() == 'celsius') {
        val = (parseFloat($('#convert-from input').val(), 10) - 32) * (5 / 9);
      } else {
        val = $('#convert-from input').val();
      }
    }
    else {
      val = Math.round((parseFloat($('#convert-from input').val().replace(',', '.'), 10) * parseFloat($('#convert-from option:selected').val(), 10) / parseFloat($('#convert-to option:selected').val(), 10)) * 1000) / 1000;
    }
    if (isNaN(val))
      val = '';
    $('#convert-to input').val(val);
  },

  /**
   * Display categories in the kitchen drawer commodities.
   *
   * @param data
   *   Data for det categories.
   */
  displayCommoditiesCategories: function(data) {
    $('#drawer-content-top ul li').remove();
    $('<li><span>' + Drupal.t('All') + '</span></li>').appendTo('#drawer-content-top ul').click(Cookbook.clickDrawerCategory).click();
    $.each(data, function(i, category) {
      $('<li id="drawer-category-' + category['tid'] + '"><span>' + category['name'] + '</span></li>').click(Cookbook.clickDrawerCategory).appendTo('#drawer-content-top ul');
    })
  },

  /**
   * Select a category in the kitchen drawer commodities.
   */
  clickDrawerCategory: function() {
    if ($(this).attr('id') == '') {
      Cookbook.loadDrawerCategory('@');
    } else {
      Cookbook.loadDrawerCategory($(this).attr('id').split('-')[2]);
    }
    $('#drawer-content-top li').removeClass('selected');
    $(this).addClass('selected');
  },

  /**
   * Load the selected category in the kitchen drawer commodities.
   *
   * @param categoryId
   *   Identifier for the category we're going to display.
   */
  loadDrawerCategory: function(categoryId) {
    $('#drawer-titles li').remove();
    $('#drawer-descriptions li').remove();
    $('#drawer-titles').append('<li>' + Cookbook.drawerLoader + '</li>');
    $.getJSON(Drupal.settings['cookbook']['url'] + '/xhr/commodities/category/' + categoryId, Cookbook.displayDrawerCategory);
  },

  /**
   * Display the selected category in the kitchen drawer commodities.
   *
   * @param data
   *   Data for the category.
   */
  displayDrawerCategory: function(data) {
    $('#drawer-titles li').remove();
    $.each(data, function(i, node) {
      $('<li>' + node['title'] + '</li>').appendTo('#drawer-titles').click(function() {
        $('#drawer-descriptions').animate({
          'scrollTop': $('#description-' + node['vid'])[0].offsetTop
        }, 500);
      });
      $('<li id="description-' + node['vid'] + '"><h3>' + node['title'] + '</h3><p class="small">' + node['teaser'] + '</p><a href="' + node['url'] + '" target="_blank">' + Drupal.t('Read more') + '</a></li>').appendTo('#drawer-descriptions').children('p:first').textOverflow({
        'lines': 4
      });
    });
    $('#drawer-titles').jScrollPane({
      'dragMinHeight': 15
    });
  },

  /**
   * Filter the kitchen drawer titles.
   *
   * @param e
   *   Event object.
   */
  filterTitles: function(e) {
    if (e.keyCode == 13) {
      $('#drawer-titles li:visible:first').click();
    } else {
      $('#drawer-titles li').each(function(i, title) {
        if ($(title).html().match(new RegExp('.*' + $('#drawer-search input').val() + '.*', 'i'))) {
          $(title).show();
        }
        else {
          $(title).hide();
        }
      });
      $('#drawer-titles').jScrollPane({
        'dragMinHeight': 15
      })[0].scrollTo(0);
    }
  },

  /**
   * Remove watermark when we focus on the kitchen drawer search field.
   */
  searchFocus: function() {
    $(this).val('').css('color', '#dcb86c').unbind('focus').unbind('blur').blur(Cookbook.searchFocusout);
  },

  /**
   * Set watermark when we're no longer focusing on the kitchen drawer search field
   */
  searchFocusout: function() {
    if ($(this).val() == '')
      $(this).val(Drupal.t('Search')).css('color', '#b19063').unbind('focus').unbind('blur').focus(Cookbook.searchFocus);
  }
}