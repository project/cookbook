
/**
 * Watermark in textfield/area
 */
jQuery.fn.watermark = function(settings) {
  settings = jQuery.extend({
    color: '#aaa',
    text: 'Enter text.',
    focus: function() {},
    blur: function() {}
  }, settings);
  return this.each(function() {
    $(this).bind('blur', function() {
      if ($(this).val() == '') {
        $(this).val(settings.text).css('color', settings.color);
        $(this).focus(function() {
          $(this).unbind('focus').bind('focus', settings.focus).val('').css('color', '#000');
          settings.focus();
        });
      }
      settings.blur();
    }).trigger('blur');
  });
}

/**
 * Cookbook container
 */
var Cookbook = {
  /**
   * Initialize special features for the recipe node form.
   */
  ready: function() {
    CookbookImages.init();

    if (Drupal.settings['cookbook']['ingredientNodes'] == 1) {
      // Init ingredient node system
      CookbookIngredients.init();
    }
    else {
      // Init simple ingredient system
      CookbookSI.init();
    }

    CookbookSteps.init();

    // Add submit handler
    $('#node-form').submit(Cookbook.submit);
  },

  /**
   * Handles form submit
   */
  submit: function() {
    CookbookImages.store();
    CookbookSteps.store();

    if (Drupal.settings['cookbook']['ingredientNodes'] == 1) {
      CookbookIngredients.store();
    }
    else {
      CookbookSI.store();
    }

    // desc textarea
    $('#edit-body').val($('#desc').val());
  },

  /**
   * Creates a button.
   *
   * @param name
   *   The name of the button.
   * @param type
   *   The button type.
   *
   * @return
   *   jQuery object containing the button.
   */
  createButton: function(name, type, text) {
    return $('<input type="button" class="form-button cb-button" id="edit-' + name + '-' + type + '" value="' + text + '"/>');
  },

  /**
   * Move item up.
   */
  up: function() {
    var $li = $(this).parent().parent();
    $li.insertBefore($li.prev());
    Cookbook.checkList($li.parent());
  },

  /**
   * Move item down.
   */
  down: function() {
    var $li = $(this).parent().parent();
    $li.insertAfter($li.next());
    Cookbook.checkList($li.parent());
  },

  /**
   * Go through each item in the given list.
   *
   * @param $list
   *   The list to go through.
   */
  checkList: function($list) {
    $list.children().each(Cookbook.checkItem);
  },

  /**
   * Check item to determine if it's first or last in the list.
   * Hide up button if it's first and hide down button if it's last.
   */
  checkItem: function() {
    var $this = $(this);
    var $up = $this.children('div:last').children('div:first');
    if ($up[0] == undefined) {
      $up = $this.children('.recipe-image-left').children();
    }
    var $down = $this.children('div:last').children('div:last');
    if ($down[0] == undefined) {
      $down = $this.children('.recipe-image-right').children();
    }

    if ($this.prev()[0] == undefined) {
      $up.hide();
      $down.css('margin-top', '12px');
    }
    else {
      $up.show();
      $down.css('margin-top', '0');
    }

    if ($this.next()[0] == undefined) {
      $down.hide();
    }
    else {
      $down.show();
    }
  },

  /**
   * Disable form submit when enter key is pressed.
   */
  disableSubmit: function(e) {
    if (e.keyCode == CookbookKeys.enter) {
      e.preventDefault();
    }
  },

  /**
   * Check a value to make sure that it isn't blank or contains any illegal chars.
   *
   * @param value
   *   The value to check.
   */
  validate: function(value) {
    if (value == '') {
      return false;
    }
    else if (value.match(/(@|;)/)) {
      alert(Drupal.t('You can not use @ or ; in the field.'));
      return false;
    }

    return true;
  }
}

/**
 * Cookbook images special features container.
 */
var CookbookImages = {
  /**
   * Initialize special features for the images field.
   */
  init: function() {
    CookbookImages.$field = $('#edit-image').keypress(Cookbook.disableSubmit);
    CookbookImages.$container = $('<div></div>').insertBefore('#edit-image');
    CookbookImages.$default = $('<img src="' + Drupal.settings['basePath'] + Drupal.settings['cookbook']['path'] + '/theme/images/default-recipe.png" alt="' + Drupal.t('Image not found!') + '" width="100" height="75" class="recipe-image-default"/>').insertBefore(CookbookImages.$container);

    // Load stored images
    if (CookbookImages.$field.val() != '') {
      var images = CookbookImages.$field.val().split('@');
      for (var i = 1; i < images.length; i++) {
        var image = images[i].split(';');
        CookbookImages.add(image[1], image[0]);
      }
    }

    // Tweak the image field
    CookbookImages.$field.css({
      'width': 'auto',
      'display': 'inline'
    }).val('').watermark({
      text: Drupal.t('Type in image url or node id and click "Add".')
    });

    // Add buttons
    CookbookImages.$add = Cookbook.createButton('image', 'add', Drupal.t('Add')).click(CookbookImages.validate).insertAfter(CookbookImages.$field);
    CookbookImages.$save = Cookbook.createButton('image', 'save', Drupal.t('Save')).click(CookbookImages.save).insertAfter(CookbookImages.$add).hide();
    CookbookImages.$cancel = Cookbook.createButton('image', 'cancel', Drupal.t('Cancel')).click(CookbookImages.reset).insertAfter(CookbookImages.$save).hide();
  },

  /**
   * Add image to container.
   */
  add: function(url, id) {
    if (id == undefined) {
      id = 0;
    }
    
    var attrs = {
      'id': id,
      'nodeId': undefined
    }
    
    // If URL is a number assume node id
    if (url == parseInt(url, 10)) {
      attrs['nodeId'] = url;
      url = Drupal.settings['imageUrl'] + '/view/' + url + '/' + Drupal.settings['cookbook']['imageSize'];
    }

    // Insert image with buttons
    var $image = $('<div class="recipe-image"></div>').data('attrs', attrs).appendTo(CookbookImages.$container);
    var $left = $('<span class="recipe-image-left"></span>').appendTo($image);
    $('<span title="' + Drupal.t('Move left') + '">&lt;</span>').appendTo($left).click(Cookbook.up);
    var $right = $('<span class="recipe-image-right"></span>').appendTo($image);
    $('<span title="' + Drupal.t('Move right') + '">&gt;</span>').appendTo($right).click(Cookbook.down);
    var $delete = $('<span class="recipe-image-delete"></span>').appendTo($image);
    $('<span title="' + Drupal.t('Delete') + '">x</span>').appendTo($delete).click(CookbookImages.remove);
    $('<img class="recipe-image-image" src="' + url + '" alt="' + Drupal.t('Image not found!') + '" title="' + Drupal.t('Edit') + '" width="100" height="75"/>').click(CookbookImages.edit).appendTo($image);

    Cookbook.checkList(CookbookImages.$container);

    // Hide default image if we have images
    if (CookbookImages.$container.children().length > 0) {
      CookbookImages.$default.hide();
    }
  },

  /**
   * Edit image in container.
   */
  edit: function() {
    // Make sure we're not editing two images at the same time.
    CookbookImages.$container.children('.editing').removeClass('editing');
    
    var $this = $(this).parent().addClass('editing');
    var attrs = $this.data('attrs');
    
    if (attrs['nodeId'] != undefined) {
      CookbookImages.$field.focus().val(attrs['nodeId']);
    }
    else {
      CookbookImages.$field.focus().val($(this).attr('src'));
    }
    CookbookImages.$add.hide();
    CookbookImages.$save.show();
    CookbookImages.$cancel.show();
  },

  /**
   * Save image after editing.
   */
  save: function() {
    var image = CookbookImage.$field.focus().val();
    if (Cookbook.validate(image)) {
      var $editing = CookbookImages.$container.children('.editing');
      var attrs = $editing.data('attrs');

      // If URL is a number assume node id
      if (image == parseInt(image, 10)) {
        attrs['nodeId'] = image;
        image = Drupal.settings['imageUrl'] + '/view/' + image;
      }
      else {
        attrs['nodeId'] = undefined;
      }

      $editing.children('img').attr('src', image);
      $editing.data('attrs', attrs);
      CookbookImages.reset();
    }
  },

  /**
   * Remove image from container.
   */
  remove: function() {
    $(this).parent().parent().remove();

    Cookbook.checkList(CookbookImages.$container);

    // Show default image if we have no images.
    if (CookbookImages.$container.children().length == 0) {
      CookbookImages.$default.show();
    }
    CookbookImages.reset();
  },

  /**
   * Reset images field.
   */
  reset: function() {
    CookbookImages.$container.children('.editing').removeClass('editing');
    CookbookImages.$field.val('').trigger('blur');
    CookbookImages.$cancel.hide();
    CookbookImages.$save.hide();
    CookbookImages.$add.show();
  },

  /**
   * Validate images field.
   */
  validate: function(e) {
    var image = CookbookImages.$field.focus().val();
    if (Cookbook.validate(image)) {
      CookbookImages.add(image);
      CookbookImages.$field.val('');
    }
  },

  /**
   * Serialize images for storage.
   */
  store: function() {
    CookbookImages.$field.val('');
    CookbookImages.$container.children('div').each(function() {
      var attrs = $(this).data('attrs');
      if (attrs['nodeId'] != undefined) {
        attrs['url'] = attrs['nodeId'];
      }
      else {
        attrs['url'] = $(this).children('img').attr('src');
      }
      CookbookImages.$field.val(CookbookImages.$field.val() + '@' + attrs['id'] + ';' + attrs['url']);
    });
  }
}

/**
 * Container for key codes.
 */
var CookbookKeys = {
  enter: 13,
  up: 38,
  down: 40
}

/**
 * Container for the cookbook's recipe ingredient picker.
 */
var CookbookIngredients = {
  /**
   * Initialize the ingredient picker.
   */
  init: function() {
    // Insert elements and bind triggers.
    CookbookIngredients.$field = $('#edit-ingredients');
    CookbookIngredients.$results = $('<ul id="ingredient-results"></ul>').insertAfter(CookbookIngredients.$field);
    CookbookIngredients.$list = $('<ul></ul>').insertBefore(CookbookIngredients.$field);
    CookbookIngredients.$amount = $('<input type="text" size="2" title="' + Drupal.t('Amount') + '" class="cb-eml" disabled/>').insertAfter(CookbookIngredients.$field).keyup(CookbookIngredients.validate).keypress(Cookbook.disableSubmit);
    CookbookIngredients.$unit = $('<select title="' + Drupal.t('Unit of measurement') + '" class="cb-eml" disabled></select>').insertAfter(CookbookIngredients.$amount).keypress(Cookbook.disableSubmit);
    CookbookIngredients.$display = $('<input type="text" size="20" title="' + Drupal.t('Display name') + '" class="cb-eml" disabled/>').insertAfter(CookbookIngredients.$unit).keypress(Cookbook.disableSubmit);
    CookbookIngredients.$add = $('<input type="button" class="form-button cb-button" value="' + Drupal.t('Add') + '" disabled/>').insertAfter(CookbookIngredients.$display);
    CookbookIngredients.$save = $('<input type="button" class="form-button cb-button" value="' + Drupal.t('Save') + '" disabled/>').insertAfter(CookbookIngredients.$add).hide();
    CookbookIngredients.$cancel = $('<input type="button" class="form-button cb-button" value="' + Drupal.t('Cancel') + '"/>').insertAfter(CookbookIngredients.$save).click(CookbookIngredients.reset).hide();

    // Load stored ingredients
    var stored = CookbookIngredients.$field.val().split('@');
    for (var i = 1; i < stored.length; i++) {
      var ingredient = stored[i].split(';');
      CookbookIngredients.insert(ingredient[0], {
        'id': ingredient[1],
        'name': ingredient[2]
      }, {
        'id': ingredient[3],
        'name': ingredient[4]
      }, ingredient[5], ingredient[6]);
    }

    // Tweak the ingredient field
    CookbookIngredients.$field.css({
      'display': 'inline-block',
      'width': '300px'
    }).val('').watermark({
      text: Drupal.t('Type in ingredient name.'),
      focus: function() {
        $(this).keyup();
      },
      blur: function() {
        CookbookIngredients.$results.fadeOut('fast');
      }
    }).keyup(CookbookIngredients.processKey).keypress(Cookbook.disableSubmit);
  },
  
  /**
   * Process key strokes for the ingredient field.
   */
  processKey: function(e) {
    if (e.keyCode != undefined || CookbookIngredients.$field.val() == '') {
      // Disable fields
      CookbookIngredients.$amount.attr('disabled', true);
      CookbookIngredients.$unit.attr('disabled', true);
      CookbookIngredients.$display.attr('disabled', true);
      CookbookIngredients.$add.attr('disabled', true);
      CookbookIngredients.$save.attr('disabled', true);
    }

    if (CookbookIngredients.$field.val() != '') {
      if (e.keyCode == CookbookKeys.enter) {
        // Select ingredient
        CookbookIngredients.$results.children('.selected').trigger('click');
        CookbookIngredients.$field.trigger('blur');
        CookbookIngredients.$amount.focus();
      }
      else if (e.keyCode == CookbookKeys.up) {
        // Highlight previous ingredient in result list
        if (CookbookIngredients.$results.children('.selected').prev()[0] != undefined) {
          CookbookIngredients.$results.children('.selected').css('background', '#fff').removeClass('selected').prev().css('background', '#a9d8ff').addClass('selected');
        }
      }
      else if (e.keyCode == CookbookKeys.down) {
        // Highlight next ingredient in result list
        if (CookbookIngredients.$results.children('.selected').next()[0] != undefined) {
          CookbookIngredients.$results.children('.selected').css('background', '#fff').removeClass('selected').next().css('background', '#a9d8ff').addClass('selected');
        }
      }
      else {
        // Search for the ingredient
        CookbookIngredients.setTimer();
      }
    }
  },

  setTimer: function() {
    clearTimeout(CookbookIngredients.timer);
    CookbookIngredients.timer = setTimeout(CookbookIngredients.search, 500);
  },

  /**
   * Search for the ingredient.
   */
  search: function() {
    $.getJSON(Drupal.settings['cookbook']['url'] + '/xhr/ingredients/' + encodeURI(CookbookIngredients.$field.val()), CookbookIngredients.processSearch);
  },

  /**
   * Processes search results.
   */
  processSearch: function(data) {
    // Remove old result
    CookbookIngredients.$results.children().remove();

    // Go through each ingredient in the new result
    $.each(data, function(n, ingredient) {
      var $li = $('<li class="result-ingredient">' + ingredient['title'] + '</li>').appendTo(CookbookIngredients.$results).click(function() {
        CookbookIngredients.select(ingredient['nid'], ingredient['title']);
      }).mouseover(CookbookIngredients.mouseoverResult).mouseout(CookbookIngredients.mouseoutResult);

      // Make the first ingredient selected by default
      if (n == 0) {
        $li.addClass('selected').css('background', '#a9d8ff');
      }

      // Show result
      CookbookIngredients.$results.fadeIn('fast');
    });

    if (CookbookIngredients.$results.children('.selected').html() != null && CookbookIngredients.$field.val().toLowerCase() == CookbookIngredients.$results.children('.selected').html().toLowerCase() && e.keyCode != undefined) {
      CookbookIngredients.$results.children('.selected').trigger('click');
    }
  },

  /**
   * Starts mouseover effect for result items
   */
  mouseoverResult: function() {
    var $this = $(this);
    if (!$this.hasClass('selected')) {
      $this.css('background', '#d9ecfc');
    }
  },

  /**
   * Ends mouseover effect for result items
   */
  mouseoutResult: function() {
    var $this = $(this);
    if (!$this.hasClass('selected')) {
      $this.css('background', '#ffffff');
    }
  },

  /**
   * Load units for the selected ingredient.
   */
  loadUnits: function(id, selected) {
    CookbookIngredients.$unit.attr('disabled', false).children().remove();
    $('<option value="0">g</option>').data('attrs', {
      'id': 0,
      'name': 'g'
    }).appendTo(CookbookIngredients.$unit);
    $.getJSON(Drupal.settings['cookbook']['url'] + '/xhr/units/' + id, function(data) {
      $.each(data, function(n, unit) {
        var select = '';
        if (unit['id'] == selected)
          select = ' selected';
        $('<option value="' + unit['id'] + '"' + select +'>' + unit['name'] + '</option>').data('attrs', {
          'id': unit['id'],
          'name': unit['name']
        }).appendTo(CookbookIngredients.$unit);
      });
    });
  },

  /**
   * Select an ingredient.
   */
  select: function(id, name) {
    CookbookIngredients.$field.val(name);

    // Enable fields
    CookbookIngredients.$amount.attr('disabled', false).keyup();
    CookbookIngredients.$display.attr('disabled', false);
    CookbookIngredients.$display.val(CookbookIngredients.$field.val().split(',')[0]);

    if (CookbookIngredients.$edit != undefined) {
      // Updating an existing ingredient
      CookbookIngredients.loadUnits(id, CookbookIngredients.$edit.data('attrs')['unit']['id']);

      CookbookIngredients.$add.hide().next().show().attr('disabled', false).unbind('click').click(function() {
        CookbookIngredients.update({
          'id': id,
          'name': name
        }, CookbookIngredients.$unit.children(':selected').data('attrs'), CookbookIngredients.$amount.val(), CookbookIngredients.$display.val().toLowerCase());
      }).next().show();
    }
    else {
      // Adding a new ingredient
      CookbookIngredients.loadUnits(id);
      
      CookbookIngredients.$add.unbind('click').click(function() {
        CookbookIngredients.insert(0, {
          'id': id,
          'name': name
        }, CookbookIngredients.$unit.children(':selected').data('attrs'), CookbookIngredients.$amount.val(), CookbookIngredients.$display.val().toLowerCase());
      });
    }
  },

  /**
   * Validate the ingredient amount.
   */
  validate: function() {
    var check = CookbookIngredients.$amount.val().replace(',', '.');
    if (check == parseFloat(check, 10)) {
      CookbookIngredients.$add.attr('disabled', false);
      CookbookIngredients.$save.attr('disabled', false);
    }
    else {
      CookbookIngredients.$add.attr('disabled', true);
      CookbookIngredients.$save.attr('disabled', true);
    }
  },

  /**
   * Insert the new ingredient.
   */
  insert: function(id, ingredient, unit, amount, display) {
    // Validate display
    if (display.match(/(@|;)/)) {
      alert(Drupal.t('You can not use @ or ; in the field.'));
      return false;
    }

    // Create list element and append to ingredients
    var $li = $('<li><span>' + amount + ' ' + unit['name'] + ' ' + display + '</span></li>').data('attrs', {
      'id': id,
      'ingredient': ingredient,
      'unit': unit,
      'amount': amount,
      'display': display
    }).appendTo(CookbookIngredients.$list);

    // Add edit and delete buttons
    $('<input type="button" class="form-button cb-button" value="' + Drupal.t('Edit') + '"/>').click(CookbookIngredients.edit).appendTo($li);
    $('<input type="button" class="form-button cb-button" value="' + Drupal.t('Delete') + '"/>').click(CookbookIngredients.remove).appendTo($li);
    var $updown = $('<div class="updown"></div>').appendTo($li);
    $('<div class="up" title="' + Drupal.t('Move up') + '"/>').click(Cookbook.up).appendTo($updown);
    $('<div class="down" title="' + Drupal.t('Move down') + '"/>').click(Cookbook.down).appendTo($updown);

    // Check list
    Cookbook.checkList(CookbookIngredients.$list);

    CookbookIngredients.reset();
  },

  /**
   * Edit an ingredient.
   */
  edit: function() {
    CookbookIngredients.$edit = $(this).parent();
    var attrs = CookbookIngredients.$edit.data('attrs');

    CookbookIngredients.$field.focus()
    CookbookIngredients.select(attrs['ingredient']['id'], attrs['ingredient']['name']);
    CookbookIngredients.$amount.val(attrs['amount']);
    CookbookIngredients.$display.val(attrs['display']);
  },

  /**
   * Remove an ingredient.
   */
  remove: function() {
    $(this).parent().remove();
    Cookbook.checkList(CookbookIngredients.$list);
  },

  /**
   * Reset the ingredient field.
   */
  reset: function() {
    CookbookIngredients.$list.children('.editing').removeClass('editing');
    CookbookIngredients.$add.show().next().hide().next().hide();
    CookbookIngredients.$field.val('').focus().next().val('').next().next().val('').prev().children().remove();
    CookbookIngredients.$edit = undefined;
  },

  /**
   * Update an ingredient.
   */
  update: function(ingredient, unit, amount, display) {
    if (display.match(/(@|;)/)) {
      alert(Drupal.t('You can not use @ or ; in the field.'));
      return false;
    }

    CookbookIngredients.$edit.data('attrs', {
      'id': CookbookIngredients.$edit.data('attrs')['id'],
      'ingredient': ingredient,
      'unit': unit,
      'amount': amount,
      'display': display
    }).children('span').html(amount + ' ' + unit['name'] + ' ' + display);

    CookbookIngredients.reset();
  },

  /**
   * Serialize the ingredients for storage.
   */
  store: function() {
    CookbookIngredients.$field.val('');
    CookbookIngredients.$list.children('li').each(function() {
      var attrs = $(this).data('attrs');
      CookbookIngredients.$field.val(CookbookIngredients.$field.val() + '@' + attrs['id'] + ';' + attrs['ingredient']['id'] + ';' + attrs['ingredient']['name'] + ';' + attrs['unit']['id'] + ';' + attrs['unit']['name'] + ';' + attrs['amount'] + ';' + attrs['display']);
    });
  }
}

var CookbookSI = {
  /**
   * Initialize simple ingredient system
   */
  init: function() {
    var $wrapper = $('#edit-ingredients-wrapper');
    CookbookSI.$field = $('#edit-ingredients').keypress(Cookbook.disableSubmit);
    CookbookSI.$list = $('<ul></ul>').insertBefore(CookbookSI.$field);

    // Load steps
    var steps = CookbookSI.$field.val().split('@');
    for (var i = 1; i < steps.length; i++) {
      CookbookSI.add(steps[i]);
    }

    CookbookSI.$field.val('').watermark({
      text: Drupal.t('Type in ingredient and click "Add".')
    });

    CookbookSI.$add = Cookbook.createButton('step', 'add', Drupal.t('Add')).click(CookbookSI.validate).appendTo($wrapper);
    CookbookSI.$save = Cookbook.createButton('step', 'save', Drupal.t('Save')).click(CookbookSI.save).appendTo($wrapper).hide();
    CookbookSI.$cancel = Cookbook.createButton('step', 'cancel', Drupal.t('Cancel')).click(CookbookSI.reset).appendTo($wrapper).hide();
  },

  /**
   * Validate the ingredient before putting it in the list.
   */
  validate: function() {
    var ingredient = CookbookSI.$field.focus().val();
    if (Cookbook.validate(ingredient)) {
      CookbookSI.add(ingredient);
      CookbookSI.$field.val('');
    }
  },

  /**
   * Add ingredient to the list.
   */
  add: function(ingredient) {
    var $li = $('<li><span>' + ingredient + '</span></li>').appendTo(CookbookSI.$list);

    Cookbook.createButton('step', 'edit', Drupal.t('Edit')).click(CookbookSI.edit).appendTo($li);
    Cookbook.createButton('step', 'delete', Drupal.t('Delete')).click(CookbookSI.remove).appendTo($li);
    var $updown = $('<div class="updown"></div>').appendTo($li);
    $('<div class="up" title="' + Drupal.t('Move up') + '"/>').click(Cookbook.up).appendTo($updown);
    $('<div class="down" title="' + Drupal.t('Move down') + '"/>').click(Cookbook.down).appendTo($updown);

    // Check list
    Cookbook.checkList(CookbookSI.$list);
  },

  /**
   * Remove ingredient from the list.
   */
  remove: function() {
    $(this).parent().remove();
    CookbookSI.reset();
    Cookbook.checkList(CookbookSI.$list);
  },

  /**
   * Begin editing an ingredient.
   */
  edit: function() {
    // Make sure we're not editing two steps at the same time.
    CookbookSI.$list.children('.editing').removeClass('editing');

    $(this).parent().addClass('editing');
    CookbookSI.$field.focus().val(($(this).prev().html()));
    CookbookSI.$add.hide();
    CookbookSI.$save.show();
    CookbookSI.$cancel.show();
  },

  /**
   * Reset the ingredient field.
   */
  reset: function() {
    CookbookSI.$list.children('.editing').removeClass('editing');
    CookbookSI.$field.val('').trigger('blur');
    CookbookSI.$cancel.hide();
    CookbookSI.$save.hide();
    CookbookSI.$add.show();
  },

  /**
   * Put the changed ingredient back in the list.
   */
  save: function() {
    var ingredient = CookbookSI.$field.focus().val();
    if (Cookbook.validate(ingredient)) {
      CookbookSI.$list.children('.editing').children('span').html(ingredient);
      CookbookSI.reset();
    }
  },

  /**
   * Serialize the ingredients for storage.
   */
  store: function() {
    CookbookSI.$field.val('');
    CookbookSI.$list.children('li').each(function() {
      CookbookSI.$field.val(CookbookSI.$field.val() + '@' + $(this).children('span:first').html());
    });
  }
}

/**
 * Container for special steps features.
 */
var CookbookSteps = {
  /**
   * Initialize special features for the steps field.
   */
  init: function() {
    var $wrapper = $('#edit-steps-wrapper');
    CookbookSteps.$field = $('#edit-steps');
    CookbookSteps.$list = $('<ol></ol>').insertAfter($wrapper.children('label'));

    // Load steps
    var steps = CookbookSteps.$field.val().split('@');
    for (var i = 1; i < steps.length; i++) {
      CookbookSteps.add(steps[i]);
    }
    CookbookSteps.$field.val('');

    CookbookSteps.$field.watermark({
      text: Drupal.t('Type in step and click "Add".')
    });

    // Add buttons
    CookbookSteps.$add = Cookbook.createButton('step', 'add', Drupal.t('Add')).click(CookbookSteps.validate).appendTo($wrapper);
    CookbookSteps.$save = Cookbook.createButton('step', 'save', Drupal.t('Save')).click(CookbookSteps.save).appendTo($wrapper).hide();
    CookbookSteps.$cancel = Cookbook.createButton('step', 'cancel', Drupal.t('Cancel')).click(CookbookSteps.reset).appendTo($wrapper).hide();
  },

  /**
   * Add step to list.
   *
   * @param step
   *   The step to add.
   */
  add: function(step) {
    step = step.replace("\n", '<br>');
    var $li = $('<li><span>' + step + '</span></li>').appendTo(CookbookSteps.$list);

    Cookbook.createButton('step', 'edit', Drupal.t('Edit')).click(CookbookSteps.edit).appendTo($li);
    Cookbook.createButton('step', 'delete', Drupal.t('Delete')).click(CookbookSteps.remove).appendTo($li);
    var $updown = $('<div class="updown"></div>').appendTo($li);
    $('<div class="up" title="' + Drupal.t('Move up') + '"/>').click(Cookbook.up).appendTo($updown);
    $('<div class="down" title="' + Drupal.t('Move down') + '"/>').click(Cookbook.down).appendTo($updown);

    // Check list
    Cookbook.checkList(CookbookSteps.$list);
  },

  /**
   * Edit step in list.
   */
  edit: function() {
    // Make sure we're not editing two steps at the same time.
    CookbookSteps.$list.children('.editing').removeClass('editing');
    
    $(this).parent().addClass('editing');
    CookbookSteps.$field.focus().val($(this).prev().html().replace('<br>', '\n'));
    CookbookSteps.$add.hide();
    CookbookSteps.$save.show();
    CookbookSteps.$cancel.show();
  },

  /**
   * Remove step from list.
   */
  remove: function() {
    $(this).parent().remove();
    CookbookSteps.reset();
    Cookbook.checkList(CookbookSteps.$list);
  },

  /**
   * Reset the step field.
   */
  reset: function() {
    CookbookSteps.$list.children('.editing').removeClass('editing');
    CookbookSteps.$field.val('').trigger('blur');
    CookbookSteps.$cancel.hide();
    CookbookSteps.$save.hide();
    CookbookSteps.$add.show();
  },

  /**
   * Save the step to the previous list item.
   */
  save: function() {
    var step = CookbookSteps.$field.focus().val();
    if (Cookbook.validate(step)) {
      CookbookSteps.$list.children('.editing').children('span').html(step);
      CookbookSteps.reset();
    }
  },

  /**
   * Validate the step field before adding the step.
   */
  validate: function() {
    var step = CookbookSteps.$field.focus().val();
    if (Cookbook.validate(step)) {
      CookbookSteps.add(step);
      CookbookSteps.$field.val('');
    }
  },

  /**
   * Serialize the steps for storage.
   */
  store: function() {
    CookbookSteps.$field.val('');
    CookbookSteps.$list.children('li').each(function() {
      CookbookSteps.$field.val(CookbookSteps.$field.val() + '@' + $(this).children('span:first').html());
    });
  }
}

// Start initilizing special features when the document is ready
$(document).ready(Cookbook.ready);