
/**
 * Init recipe page
 */
$(document).ready(function() {
  // Hide all images
  $('#recipe-image img').hide();

  // Load and center the first image
  var $image = $('#recipe-image img:first-child');
  if ($image[0].complete) {
    // Image is cached
    $image.show();
    Cookbook.centerImage($image);
  }
  else {
    $image.load(function() {
      // Image loaded
      $(this).show();
      Cookbook.centerImage(this);
    });
  }

  // Change image every 5 seconds
  if ($('#recipe-image img').length > 1) {
    $('#recipe-image-overlay').show().children(':first').click(function() {
      Cookbook.stopSlideshow();
      Cookbook.previousImage();
    }).parent().children(':last').click(function() {
      Cookbook.stopSlideshow()
      Cookbook.nextImage();
    });
    $('#recipe-image-overlay-2').show()
    Cookbook.startSlideshow();
  }

  // Pause slideshow when pause icon is clicked
  $('#recipe-image-overlay-2').click(Cookbook.stopSlideshow);

  // Position and show term description on mouseover
  $('.term').mouseover(function() {
    var fix = 0;
    if ($(this)[0].offsetTop > 0)
      fix = $(this)[0].offsetTop;
    $(this).next().show().css('left', $(this)[0].offsetLeft - ($(this).next().width() / 2) + ($(this)[0].offsetWidth / 2) ).css('top', $(this)[0].offsetTop - 14 - $(this).next()[0].offsetHeight - fix)
    .next().show().css('left', $(this)[0].offsetLeft - ($(this).next().next().width() / 2) + ($(this)[0].offsetWidth / 2)).css('top', $(this)[0].offsetTop - 15 - fix);
  }).mouseout(function() {
    $(this).next().hide().next().stop().hide();
  });

  // Calculate the amount of each ingredient when servings change
  $('#number-of-servings').keyup(function() {
    var servings = $('#number-of-servings').val().replace(',','.');
    if (servings == parseFloat(servings, 10)) {
      $('.ingredient-amount').each(function() {
        $(this).html(Math.round($(this).next().html() * 1000 * servings) / 1000);
      });
    }
  });

  // Show the rest of the nutrients
  if ($('#nutrition-facts table tr').length > 4) {
    $('.show-more').show().click(Cookbook.showMore);
  }

  $('#print').click(function() {
    window.print();
  });
});

/**
 * Center recipe image
 *
 * @param image
 *  The image to center
 */
Cookbook.centerImage = function(image) {
  $('#recipe-image div')[0].scrollTop = ($(image).height() / 2) - 125;
}

/**
 * Display the next recipe image
 */
Cookbook.nextImage = function() {
  // Fade out overlay and current image
  $('#recipe-image-overlay-2').fadeOut('fast');
  $('#recipe-image-overlay').fadeOut('fast', function() {
    $('#recipe-image img:first-child').fadeOut('fast', function() {
      // Update the overlay's image number
      var num = parseInt($('#recipe-image-overlay .num').html(), 10) + 1
      if (num > $('#recipe-image img').length) {
        num = 1;
      }
      $('#recipe-image-overlay .num').html(num);

      // Move current image to the bottom of the list.
      $(this).appendTo($(this).parent());

      // Fade in new image and updated overlay
      $('#recipe-image img:first-child').fadeIn('slow', function() {
        $('#recipe-image-overlay').fadeIn('fast');
        $('#recipe-image-overlay-2').fadeIn('fast');
      });
      Cookbook.centerImage($('#recipe-image img:first-child'));
    });
  });
}

/**
 * Display the previous recipe image
 */
Cookbook.previousImage = function() {
  // Fade out overlay and current image
  $('#recipe-image-overlay-2').fadeOut('fast');
  $('#recipe-image-overlay').fadeOut('fast', function() {
    $('#recipe-image img:first-child').fadeOut('fast', function() {
      // Update the overlay's image number
      var num = parseInt($('#recipe-image-overlay .num').html(), 10) - 1
      if (num < 1) {
        num = $('#recipe-image img').length;
      }
      $('#recipe-image-overlay .num').html(num);

      // Insert the last image before the current
      $('#recipe-image img:last-child').insertBefore(this);

      // Fade in new image and updated overlay
      $('#recipe-image img:first-child').fadeIn('slow', function() {
        $('#recipe-image-overlay').fadeIn('fast');
        $('#recipe-image-overlay-2').fadeIn('fast');
      });
      Cookbook.centerImage($('#recipe-image img:first-child'));
    });
  });
}

/**
 * Show the rest of the nutrients.
 */
Cookbook.showMore = function() {
  $(this).html(Drupal.t('Show less')).unbind('click').click(Cookbook.showLess);
  
  if ($.browser.msie) {
    // IE needs some help (as usual)
    $('#nutrition-facts').stop().animate({
      'height': ($('#nutrition-facts div table')[0].offsetHeight + 100) + 'px'
    }, 1000);
  }
  $('#nutrition-facts div').stop().animate({
    'height': $('#nutrition-facts div table')[0].offsetHeight + 'px'
  }, 1000);
}

/**
 * Show only the top 4 nutrients.
 */
Cookbook.showLess = function() {
  $(this).html(Drupal.t('Show more')).unbind('click').click(Cookbook.showMore);

  if ($.browser.msie) {
    // Helping IE once again
    $('#nutrition-facts').stop().animate({
      'height': '170px'
    }, 1000);
  }
  $('#nutrition-facts div').stop().animate({
    'height': '70px'
  }, 1000);
}

/**
 * Stop the slideshow
 */
Cookbook.stopSlideshow = function() {
  clearInterval(Cookbook.interval);
  $('#recipe-image-overlay-2').css('background-image', $('#recipe-image-overlay-2').css('background-image').replace('pause.png', 'play.png')).unbind('click').click(Cookbook.startSlideshow);
}

/**
 * Start the slideshow
 */
Cookbook.startSlideshow = function() {
  Cookbook.interval = setInterval(Cookbook.nextImage, 10000);
  $('#recipe-image-overlay-2').css('background-image', $('#recipe-image-overlay-2').css('background-image').replace('play.png', 'pause.png')).unbind('click').click(Cookbook.stopSlideshow);
}