
/**
 * Init front page
 */
$(document).ready(function() {
  // Click on random recipe overlay
  $('#recipes div.overlay').css('cursor', 'pointer').click(function() {
    window.location = $('#recipes a').attr('href');
  });

  CookbookFrontSlideshow.init();
});

/**
 * Change random recipe on the front page.
 */
var CookbookFrontSlideshow = {
  init: function() {
    CookbookFrontSlideshow.$overlay = $('#recipes .overlay');
    CookbookFrontSlideshow.$imageContainer = $('#recipes .image');
    CookbookFrontSlideshow.$header = $('#recipes h3');
    CookbookFrontSlideshow.$link = $('#recipes a');
    CookbookFrontSlideshow.fetch();
  },
  fetch: function() {
    $.getJSON(Drupal.settings['cookbook']['url'] + '/xhr/random', CookbookFrontSlideshow.load);
  },
  load: function(data) {
    var cs = CookbookFrontSlideshow;
    cs.images = data;
    cs.nextImage = 0;
    setInterval(cs.changeImage, 10000);
  },
  changeImage: function() {
    var cs = CookbookFrontSlideshow;
    var src = cs.images[cs.nextImage]['image'];
    var title = cs.images[cs.nextImage]['title'];
    var nid = cs.images[cs.nextImage]['nid'];
    cs.nextImage++;
    if (cs.nextImage >= cs.images.length) {
      cs.nextImage = 0;
    }
    var $newImage = cs.$imageContainer.children(':first-child').clone().appendTo(cs.$imageContainer).hide().attr('src', src);
    cs.$overlay.fadeOut('fast', function() {
      $newImage.prev().fadeOut('fast', function() {
        $(this).remove();
        cs.$header.html(title);
        cs.$link.attr('href', $('#recipes a').attr('href').replace(/\d+$/, nid));
        $newImage.fadeIn('slow', function() {
          cs.$overlay.fadeIn('fast');
        });
      });
    });
  }
}