
/**
 * Init search results page
 */
$(document).ready(function() {
  // Make multiple pages with sub category tabs
  $('#sub-categories').css('height', '21px');

  // Show/hide appropriate sub sub and sub sub sub categories
  if ($('#sub-sub-categories .active').length == 0) {
    $('#sub-sub-categories td.show-all').addClass('active');
    $('#sub-sub-categories-wrapper').removeClass('hidden');
  }
  if ($('#sub-sub-sub-categories .active').length == 0) {
    $('#sub-sub-sub-categories td.show-all').addClass('active');
    $('#sub-sub-sub-categories-wrapper').removeClass('hidden');
  }

  // Button for scrolling to the next/previous "page" of tabs
  $('#tab-more').click(Cookbook.more);

  // Page select
  $('.page').click(Cookbook.selectPage);

  // Main categories
  $('#categories td').mouseover(Cookbook.mouseoverMainCategory)
  .mouseout(Cookbook.mouseoutMainCategory).click(Cookbook.selectMainCategory);

  // Sub categories
  $('#sub-categories li').mouseover(Cookbook.mouseoverSubCategory)
  .mouseout(Cookbook.mouseoutSubCategory).click(Cookbook.selectSubCategory);

  // Sub sub categories
  $('#sub-sub-categories td').mouseover(Cookbook.mouseoverSubCategory)
  .mouseout(Cookbook.mouseoutSubCategory).click(Cookbook.selectSubSubCategory);

  // Sub sub sub categories
  $('#sub-sub-sub-categories td').mouseover(Cookbook.mouseoverSubCategory)
  .mouseout(Cookbook.mouseoutSubCategory)
  .click(Cookbook.selectSubSubSubCategory);

  // Clear search keywords
  $('.remove a').click(Cookbook.clearSearchKeywords);

  // Click on one of the sorting buttons
  $('#results-order ul li').mouseover(Cookbook.mouseoverOrder)
  .mouseout(Cookbook.mouseoutOrder).click(Cookbook.setOrder);

  // Load the current hash
  Cookbook.loadHash();

  // Check if the tabs need a more button
  Cookbook.checkTabs();
});

/**
 * Get current parameters
 */
Cookbook.getParams = function() {
  var params = new Array();

  // Get category
  params[0] = Cookbook.getCategory();

  // Get search word
  params[1] = window.location.href.split('/');
  params[1] = params[1][params[1].length - 3];

  // Get order
  params[2] = Cookbook.getOrder();

  // Get page
  params[3] = parseInt($('.pages .selected').html(), 10) - 1;
  if (isNaN(params[3]))
    params[3] = parseInt($('.pages .selected').children().html(), 10) - 1;

  return params;
}

/**
 * Set hash with given parameters.
 *
 * @param params
 *   The parameters to set in the hash.
 */
Cookbook.setHash = function(params) {
  window.location.hash = '#' + params[0] + '/' + params[1] + '/' + params[2] + '/' + params[3];
}

/**
 * Set the given category as current.
 *
 * @param category
 *   The category that should be set as current.
 */
Cookbook.setCategory = function(category) {
  if (!category.hasClass('categories') && category.attr('class') != undefined) {
    Cookbook.setCategory($('#' + category.attr('class').split(' ')[1]));
  }
  category.trigger('click', [true]);
}

/**
 * Set the given order as current.
 *
 * @param order
 *   The order that should be set as current.
 */
Cookbook.setOrder = function(order) {
  order = order.split('-');
  $('#results-order ul li').removeClass('selected').removeClass('reverse');
  $('#order-' + order[0]).addClass('selected');
  if (order[1] == 'DESC')
    $('#order-' + order[0]).addClass('reverse');
}

/**
 * Set the given page as current.
 *
 * $param page
 *   The page that should be set as current.
 */
Cookbook.setPage = function(page) {
  $('.pages .selected').removeClass('selected');
  $('<span class="selected" style="display: none">' + page + '</span>').appendTo('.pages');
}

/**
 * Load the current hash.
 */
Cookbook.loadHash = function() {
  if (window.location.hash != '' && window.location.hash != '#') {
    var hash = window.location.href.split('#');
    if (hash.length == 2) {
      hash = hash[1].split('/');

      if (hash.length == 4) {
        $('#categories .selected').removeClass('selected');
        Cookbook.setCategory($('#category-' + hash[0]));
        Cookbook.setOrder(hash[2]);
        Cookbook.setPage(parseInt(hash[3], 10) + 1);
        Cookbook.loadRecipes(hash);
      }
    }
  }
}

/**
 * Load recipes.
 *
 * @param params
 *   The params to use when loading recipes.
 */
Cookbook.loadRecipes = function(params) {
  $('#results-recipes').hide();
  $('#results-loading').show();

  if (params == undefined) {
    // Get params
    var params = Cookbook.getParams();

    // Set hash
    Cookbook.setHash(params);
  }

  // Get new recipes
  $.get(Drupal.settings['cookbook']['url'] + '/xhr/results/' + params[0] + '/' + params[1] + '/' + params[2] + '/' + params[3], Cookbook.showRecipes);
}

/**
 * Show recipes.
 *
 * @param data
 *   Data for the recipes.
 */
Cookbook.showRecipes = function(data) {
  $('#results-loading').hide();
  $('#results-recipes').html(data).show();
  
  $('.reverse-fading').textColorFade({
    'color': '#330000'
  });

  $('.result-text p').each(function() {
    if (parseInt($(this).prev().css('height'), 10) > 23) {
      $(this).textOverflow({
        lines: 1,
        ellipsis: ' (...)'
      });
    }
    else {
      $(this).textOverflow({
        lines: 2,
        ellipsis: ' (...)'
      });
    }
  });

  // Pagination
  var pages = (parseInt($('#hits').html(), 10) / 6);
  if (pages - parseInt(pages, 10) < 0.5)
    pages++;
  pages = parseInt(pages, 10);
  $('span.hits').html($('#hits').html());
  var page = parseInt($('#page').html(), 10) + 1;
  var parent = $('.pages').html('');
  var span = $('<span class="page" style="cursor: pointer"></span>');
  if (page > 3)
    span.clone().html(1).appendTo(parent).click(Cookbook.selectPage).textColorFade();
  if (page > 4)
    span.clone().html('...').attr('class', '').css('cursor', 'auto').appendTo(parent);
  if (page > 2)
    span.clone().html(page - 2).appendTo(parent).click(Cookbook.selectPage).textColorFade();
  if (page > 1)
    span.clone().html(page - 1).appendTo(parent).click(Cookbook.selectPage).textColorFade();
  span.clone().html(page).attr('class', 'selected').css('cursor', 'auto').appendTo(parent);
  if (page < pages - 1)
    span.clone().html(page + 1).appendTo(parent).click(Cookbook.selectPage).textColorFade();
  if (page < pages - 2)
    span.clone().html(page + 2).appendTo(parent).click(Cookbook.selectPage).textColorFade();
  if (page < pages - 3)
    span.clone().html('...').attr('class', '').css('cursor', 'auto').appendTo(parent);
  if (page < pages)
    span.clone().html(pages).appendTo(parent).click(Cookbook.selectPage).textColorFade();
}

/**
 * Selecting a new page.
 *
 * @return false
 */
Cookbook.selectPage = function() {
  $('.pages .selected').removeClass('selected');
  $(this).addClass('selected');
  Cookbook.loadRecipes();
  return false;
}

/**
 * Show next page with sub category tabs.
 */
Cookbook.more = function() {
  $('#sub-categories').scrollTop($('#sub-categories').scrollTop() + 23).css('margin', '0 12px 0 ' + (22 + $(this)[0].offsetWidth) + 'px');
  $(this).css('margin-left', '15px').attr('class', 'right').attr('class', 'left').unbind('click').click(Cookbook.less);
}

/**
 * Show previous page with sub category tabs.
 */
Cookbook.less = function() {
  $('#sub-categories').scrollTop($('#sub-categories').scrollTop() - 23).css('margin', '0 ' + (22 + $(this)[0].offsetWidth) + 'px 0 12px');
  $(this).css('margin-left', (923 - $(this)[0].offsetWidth) + 'px').attr('class', 'right').unbind('click').click(Cookbook.more);
}

/**
 * Check if we have multiple pages with sub category tabs.
 */
Cookbook.checkTabs = function() {
  $('#sub-categories').scrollTop(0).css('margin', '0 12px 0 12px');
  if ($('#sub-categories').attr('scrollHeight') > 23) {
    $('#tab-more').show().attr('class', 'right').css('margin-left', (923 - $('#tab-more')[0].offsetWidth) + 'px');
    $('#sub-categories').css('margin', '0 ' + (22 + $('#tab-more')[0].offsetWidth) + 'px 0 12px')
  } else {
    $('#tab-more').hide();
  }
}

/**
 * Select a main category
 *
 * @param e Event
 * @param dontLoad Don't load new recipes
 */
Cookbook.selectMainCategory = function(e, dontLoad) {
  // Check if we are deselecting
  var deselect = $(this).hasClass('selected');

  // Remove current highlight and highlight the new category
  $('#categories td.selected').removeClass('selected').children('a').stop().css('color', '#bbbbbb');
  if (!deselect) {
    $(this).addClass('selected');
  }

  // Show the appropriate sub categories
  $('#sub-categories li.show').removeClass('show');
  if (!deselect)
    $('#sub-categories li.category-' + $(this).attr('id').split('-')[1]).addClass('show');

  // Set show all as the active sub category
  $('#sub-categories li.active').removeClass('active').children('a').stop().css('color', '#777777');
  $('#sub-categories li.show-all').addClass('active').children('a').stop().css('color', '#ffffff');

  // Hide any sub-sub og sub-sub-sub categories
  $('#sub-sub-categories').hide();
  $('#sub-sub-sub-categories').hide();

  // Check if the tabs need a more button
  Cookbook.checkTabs();

  // Load new recipes
  if (!dontLoad)
    Cookbook.loadRecipes();
  return false;
}

/**
 * Select a sub category
 *
 * @param e Event
 * @param dontLoad Don't load new recipes
 */
Cookbook.selectSubCategory = function(e, dontLoad) {
  // Hightlight active sub category
  $('#sub-categories li.active').removeClass('active').children('a').stop().css('color', '#777777');
  $(this).addClass('active').children('a').stop().css('color', '#ffffff');

  // Show/hide any sub-sub categories
  var subSubCategories = $('#sub-sub-categories td.category-' + $(this).attr('id').split('-')[1]);
  if(subSubCategories.length > 1) {
    $('#sub-sub-categories').show();
    $('#sub-sub-categories td.show').removeClass('show');
    // Set show-all as active
    $('#sub-sub-categories td.active').removeClass('active').children('a').stop().css('color', '#666666');
    $('#sub-sub-categories td.show-all').addClass('active').children('a').stop().css('color', '#A62721');
    subSubCategories.addClass('show');
  } else {
    // Hide
    $('#sub-sub-categories').hide();
  }

  // Hide any sub-sub-sub categories
  $('#sub-sub-sub-categories').hide();

  // Load new recipes
  if (!dontLoad)
    Cookbook.loadRecipes();
  return false;
}

/**
 * Select a sub sub category
 *
 * @param e Event
 * @param dontLoad Don't load new recipes
 */
Cookbook.selectSubSubCategory = function(e, dontLoad) {
  // Highlight active sub-sub category
  $('#sub-sub-categories td.active').removeClass('active').children('a').stop().css('color', '#666666');
  $(this).addClass('active').children('a').stop().css('color', '#A62721');

  // Show/hide any sub-sub-sub categories
  var subSubSubCategories = $('#sub-sub-sub-categories td.category-' + $(this).attr('id').split('-')[1]);
  if(subSubSubCategories.length > 1) {
    $('#sub-sub-sub-categories').show();
    $('#sub-sub-sub-categories td.show').removeClass('show');
    $('#sub-sub-sub-categories td.active').removeClass('active').children('a').stop().css('color', '#666666');
    $('#sub-sub-sub-categories td.show-all').addClass('active').children('a').stop().css('color', '#A62721');
    subSubSubCategories.addClass('show');
  } else {
    $('#sub-sub-sub-categories').hide();
  }

  // Load new recipes
  if (!dontLoad)
    Cookbook.loadRecipes();
  return false;
}

/**
 * Select a sub sub sub category
 *
 * @param e Event
 * @param dontLoad Don't load new recipes
 */
Cookbook.selectSubSubSubCategory = function(e, dontLoad) {
  $('#sub-sub-sub-categories td.active').removeClass('active').children('a').stop().css('color', '#666666');
  $(this).addClass('active').children('a').stop().css('color', '#A62721');

  // Load new recipes
  if (!dontLoad)
    Cookbook.loadRecipes();
  return false;
}

/**
 * Mouseover for main categories.
 */
Cookbook.mouseoverMainCategory = function() {
  $(this).children('a').stop().animate({
    'color': '#990000'
  }, 500);
}

/**
 * Mouseout for main categories.
 */
Cookbook.mouseoutMainCategory = function() {
  if ($(this).hasClass('selected')) {
    $(this).children('a').stop().animate({
      'color': '#666666'
    }, 500);
  }
  else {
    $(this).children('a').stop().animate({
      'color': '#bbbbbb'
    }, 500);
  }
}

/**
 * Mouseover for sub, sub sub and sub sub sub categories
 */
Cookbook.mouseoverSubCategory = function() {
  if (!$(this).hasClass('active')) {
    $(this).children('a').stop().animate({
      'color': '#990000'
    }, 500);
  }
}

/**
 * Mouseout for sub, sub sub and sub sub sub categories
 */
Cookbook.mouseoutSubCategory = function() {
  if (!$(this).hasClass('active')) {
    $(this).children('a').stop().animate({
      'color': '#666666'
    }, 500);
  }
}

/**
 * Clear all search keywords
 */
Cookbook.clearSearchKeywords = function() {
  var params = Cookbook.getParams();
  params[1] = '0';
  var url = $(this).attr('href').split('/');
  for (var i = 1; i < 5; i++)
    url[url.length - i] = params[params.length - i];
  $(this).attr('href', url.join('/'));
}

/**
 * Mouseover for order links.
 */
Cookbook.mouseoverOrder = function() {
  $(this).children('a').stop().animate({
    'color': '#990000'
  }, 500);
}

/**
 * Mouseout for order links.
 */
Cookbook.mouseoutOrder = function() {
  if ($(this).hasClass('selected')) {
    $(this).children('a').stop().animate({
      'color': '#666666'
    }, 500);
  }
  else {
    $(this).children('a').stop().animate({
      'color': '#999999'
    }, 500);
  }
}

/**
 * Set the sorting order.
 *
 * @return false
 */
Cookbook.setOrder = function() {
  if ($(this).hasClass('selected')) {
    if ($(this).hasClass('reverse'))
      $(this).removeClass('reverse');
    else
      $(this).addClass('reverse');
  } else {
    $('#results-order ul li.selected').removeClass('selected').children('a').stop().css('color', '#999999');
    $(this).addClass('selected');
  }

  // Load recipes in new order
  Cookbook.loadRecipes();
  return false;
}