
/**
 * Container for cookbook functions.
 */
var Cookbook = {
  /**
   * Init special features for the ingredient node form.
   */
  init: function() {
    CookbookUnits.init();

    // Handle submit
    $('#node-form').submit(CookbookUnits.store);
  },
  
  /**
   * Creates a button.
   *
   * @param name
   *   The name of the button.
   *
   * @return
   *   jQuery object containing the button.
   */
  createButton: function(name, text) {
    return $('<input type="button" class="form-button" id="edit-unit-' + name + '" style="margin: 0 0 0 10px" value="' + text + '"/>');
  }
}

/**
 * Units field special features container.
 */
var CookbookUnits = {
  /**
   * Initialize special features on the units field.
   */
  init: function() {
    // Do some tweaking on the input field
    CookbookUnits.$field = $('#edit-units').css({
      'width': '40px',
      'display': 'inline'
    }).keypress(CookbookUnits.disableSubmit);
    CookbookUnits.$list = $('<ul></ul>').insertBefore(CookbookUnits.$field);
    $('<span>1 </span>').insertAfter(CookbookUnits.$list);

    // Load serialized units into list
    var units = CookbookUnits.$field.val().split('@');
    for (var i = 1; i < units.length; i++) {
      var unit = units[i].split(';');
      CookbookUnits.add(unit[0], unit[1], unit[2]);
    }
    CookbookUnits.$field.val('');

    // Add another input field
    CookbookUnits.$amount = $('<input type="text" class="form-text" style="width: 40px; display: inline"/>').insertAfter(CookbookUnits.$field).keypress(CookbookUnits.disableSubmit);
    $('<span> = </span>').insertAfter(CookbookUnits.$field);

    // Add buttons
    CookbookUnits.$cancel = Cookbook.createButton('cancel', Drupal.t('Cancel')).click(CookbookUnits.reset).insertAfter(CookbookUnits.$amount).hide();
    CookbookUnits.$save = Cookbook.createButton('save', Drupal.t('Save')).click(CookbookUnits.save).insertAfter(CookbookUnits.$amount).hide();
    CookbookUnits.$add = Cookbook.createButton('add', Drupal.t('Add')).click(CookbookUnits.validate).insertAfter(CookbookUnits.$amount);
    $('<span> ' + Drupal.t('grams') + '</span>').insertAfter(CookbookUnits.$amount);
  },

  /**
   * Add unit to the unit list.
   *
   * @param id
   *   Unit identifier.
   * @param unit
   *   Unit name.
   * @param grams
   *   Unit grams.
   *
   * @return
   *   true on success, false on failure.
   */
  add: function(id, unit, grams) {
    if (unit == '' || grams != parseFloat(grams, 10))
      return false;
    var $li = $('<li class="unit-' + id + '">1 <span class="unit">' + unit + '</span> = <span class="grams">' + grams + '</span> ' + Drupal.t('grams') + '</li>').appendTo(CookbookUnits.$list);
    Cookbook.createButton('edit', Drupal.t('Edit')).click(CookbookUnits.edit).appendTo($li);
    Cookbook.createButton('delete', Drupal.t('Delete')).click(CookbookUnits.remove).appendTo($li);

    return true;
  },

  /**
   * Remove unit.
   */
  remove: function() {
    if (confirm(Drupal.t('All recipes using this ingredient with this unit of measurement will now use grams instead. Are you sure you wish to continue?'))) {
      $(this).parent().remove();
    }
  },
  /**
   * Edit the unit.
   */
  edit: function() {
    var $li = $(this).parent();
    $li.addClass('editing');
    CookbookUnits.$field.val($li.children('span:first').html());
    CookbookUnits.$amount.val($li.children('span:last').html());
    CookbookUnits.$add.hide();
    CookbookUnits.$save.show();
    CookbookUnits.$cancel.show();
  },

  /**
   * Reset the unit fields.
   */
  reset: function() {
    CookbookUnits.$list.children('.editing').removeClass('editing');
    CookbookUnits.$save.hide();
    CookbookUnits.$cancel.hide();
    CookbookUnits.$add.show();
    CookbookUnits.$field.val('');
    CookbookUnits.$amount.val('');
  },

  /**
   * Put the modified unit back in the unit list.
   */
  save: function() {
    var li = CookbookUnits.$list.children('.editing');
    var unit = CookbookUnits.$field.val();
    var grams = CookbookUnits.$amount.val().replace(',', '.');
    if (unit != '' && grams == parseFloat(grams, 10)) {
      li.children('span:first').html(unit);
      li.children('span:last').html(grams);
      CookbookUnits.reset();
    }
  },

  /**
   * Validates the unit fields and adds the unit to the unit list.
   */
  validate: function() {
    if (CookbookUnits.add(0, CookbookUnits.$field.val(), CookbookUnits.$amount.val().replace(',', '.'))) {
      CookbookUnits.$field.val('');
      CookbookUnits.$amount.val('');
    }
  },

  /**
   * Serialize the units for storage
   */
  store: function() {
    CookbookUnits.$field.val('');
    CookbookUnits.$list.children('li').each(function() {
      CookbookUnits.$field.val(CookbookUnits.$field.val() + '@' + $(this).attr('class').split('-')[1] + ';' + $(this).children('span:first').html() + ';' + $(this).children('span:last').html());
    });
  },

  /**
   * Handles enter key presses.
   */
  disableSubmit: function(e) {
    // Disable form submit on enter key
    if (e.keyCode == 13) {
      CookbookUnits.validate();
      e.preventDefault();
    }
  }
}

// Initialize the cookbook when the document is ready
$(document).ready(Cookbook.init);