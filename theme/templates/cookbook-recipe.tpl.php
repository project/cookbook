<?php
?>
<div class="block">
  <div class="content">
    <h3 id="recipe-header"><?php print check_plain($recipe->title) ?></h3>
  </div>
</div>
<div class="block">
  <h2><?php print t('Recipe') ?></h2>
  <?php if ($recipe->history != ''): ?>
    <h2 class="link short-margin">
      <a href="<?php print url('cookbook/history/' . $category . '/' . $search . '/' . $order . '/' . $page . '/' . $recipe->nid) ?>" class="fading"><?php print t('History') ?></a>
    </h2>
  <?php endif;
  if ($recipe->video != ''): ?>
    <h2 class="link short-margin">
      <a href="<?php print url('cookbook/video/' . $category . '/' . $search . '/' . $order . '/' . $page . '/' . $recipe->nid) ?>" class="fading"><?php print t('Video') ?></a>
    </h2>
  <?php endif ?>
  <div class="content">
    <div id="mainbar">
      <div id="recipe-image">
        <span id="recipe-image-overlay">
          <span class="previous">&lt;</span> 
          <?php print t('Picture') ?>
          <span class="num">1</span>
          <?php print t('of') . ' ' . count($recipe->images) ?>
          <span class="next">&gt;</span>
        </span>
        <span id="recipe-image-overlay-2"></span>
        <div>
          <?php foreach ($recipe->images as $image): ?>
            <img src="<?php print check_url($image->source) ?>" alt="" width="654"/>
          <?php endforeach ?>
        </div>
      </div>
      <div id="teaser"><?php print check_plain($recipe->body) ?></div>
      <!-- RSPEAK_START -->
      <div id="ingredients">
        <h3><?php print t('Ingredients') ?></h3>
        <ul>
          <?php if (is_array($recipe->ingredients)):
            foreach ($recipe->ingredients as $ingredient): ?>
              <li><?php print $ingredient ?></li>
            <?php endforeach;
          endif ?>
        </ul>
      </div>
      <div id="steps">
        <h3><?php print t('Steps') ?></h3>
        <ol>
          <?php foreach ($recipe->steps as $step): ?>
            <li>
              <span><?php print $step ?></span>
            </li>
          <?php endforeach; ?>
        </ol>
      </div>
      <!-- RSPEAK_STOP -->
    </div>
    <div id="sidebar">
      <div id="time-diff" class="sidebar">
        <div>
          <?php print taxonomy_image_display($recipe->time_tid) ?><br/>
          <?php print check_plain($recipe->time); ?>
        </div>
        <div>
          <?php print taxonomy_image_display($recipe->diff_tid) ?><br/>
          <?php print check_plain($recipe->diff); ?>
        </div>
      </div>
      <div id="servings" class="sidebar">
        <h3><?php print t('Number of servings') ?></h3>
        <input type="text" name="servings" value="4" size="1" class="textfield" id="number-of-servings"/>
      </div>
      <?php if (variable_get('cookbook_use_ingredient_nodes', 0) == 1): ?>
        <div id="nutrition-facts" class="sidebar">
          <h3><?php print t('Nutrition facts') ?></h3>
          <div>
            <table>
              <?php $counter = 0; foreach (cookbook_get_nutrients (FALSE) as $name => $nutrient):
                if ($recipe->$name != -1): ?>
                  <tr title="<?php print $recipe->$name . ' ' . $nutrient[1] ?>"<?php if ($counter > 3): print ' class="additional"'; endif ?>>
                    <td class="option">
                      <span><?php print $nutrient[0] ?></span>
                    </td>
                    <td class="value"><?php print $recipe->$name . ' ' . $nutrient[2] ?></td>
                  </tr>
                <?php endif; $counter++;
              endforeach ?>
            </table>
          </div>
          <p class="show-more"><?php print t('Show more') ?></p>
          <p><?php print t('Estimated values per serving.') ?></p>
          <?php $ref = variable_get('cookbook_nutrition_data_reference', '');
          if ($ref != ''): ?>
            <p><?php print $ref ?></p>
          <?php endif ?>
        </div>
      <?php endif; 
      if (isset($block)): ?>
        <div id="custom-block" class="sidebar">
          <?php if ($block['subject'] != ''): ?>
            <h3><?php print $block['subject'] ?></h3>
          <?php endif ?>
          <?php if ($block['content'] != ''): ?>
            <div><?php print $block['content'] ?></div>
          <?php endif ?>
        </div>
      <?php endif ?>
    </div>
    <div id="print"></div>
  </div>
  <div class="tab-bottom">
    <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/' . $order . '/' . $page) ?>" class="fading"><?php print t('Back to recipes') ?></a>
  </div>
</div>