<?php
?>
<div class="block">
  <div class="content">
    <h3 id="recipe-header"><?php print check_plain($recipe->title) ?></h3>
  </div>
</div>
<div class="block">
  <h2 class="link">
    <a href="<?php print url('cookbook/recipe/' . $category . '/' . $search . '/' . $order . '/' . $page . '/' . $recipe->nid) ?>"><?php print t('Recipe') ?></a>
  </h2>
  <?php if ($recipe->history != ''): ?>
    <h2 class="link short-margin">
      <a href="<?php print url('cookbook/history/' . $category . '/' . $search . '/' . $order . '/' . $page . '/' . $recipe->nid) ?>"><?php print t('History') ?></a>
    </h2>
  <?php endif ?>
  <h2 class="short-margin"><?php print t('Video') ?></h2>
  <div class="content"><?php print check_markup($recipe->video, 2, false) ?></div>
  <div class="tab-bottom">
    <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/' . $order . '/' . $page) ?>"><?php print t('Back to recipes') ?></a>
  </div>
</div>
