<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
  <body>
    <div id="wrapper">
      <?php print $messages ?>
      <div id="header">
        <div>
          <?php if (variable_get('cookbook_logo', '') != ''): ?>
            <a href="<?php print url('<front>') ?>"><img src="<?php print variable_get('cookbook_logo', '') ?>" alt=""/></a>
          <?php endif;
          if (isset($top_block)): ?>
            <span id="top-block"><?php print $top_block['content'] ?></span>
          <?php endif ?>
          <h1>
            <a href="<?php print url('cookbook') ?>" class="fading"><?php print t('The Cookbook') ?></a>
          </h1>
          <p><?php print t('Find recipes that suits you in 1-2-3!') ?></p>
        </div>
      </div>
      <div id="drawer-wrapper">
        <div id="drawer">
          <div id="drawer-inside">
            <div id="drawer-content" class="drawer-content">
              <div id="drawer-content-top">
                <p><?php print t('Back') ?></p>
                <h3><?php print t('Content') ?></h3>
                <ul></ul>
              </div>
              <div id="drawer-content-content"><img src="<?php print base_path() . drupal_get_path('module', 'cookbook'); ?>/theme/images/loading-drawer.gif"/></div>
            </div>
            <div id="dictionary" class="drawer-content drawer-menu">
              <div class="icon"></div>
              <h3><?php print t('Dictionary') ?></h3>
              <p><?php print t('Look up different words and expressions related to cooking.') ?></p>
            </div>
            <div id="measurement" class="drawer-content drawer-menu">
              <div class="icon"></div>
              <h3><?php print t('Units of measurement') ?></h3>
              <p><?php print t('Units of measurement is an important part of cooking. Learn more here.') ?></p>
            </div>
            <div id="commodities" class="drawer-content drawer-menu">
              <div class="icon"></div>
              <h3><?php print t('Commodities') ?></h3>
              <p><?php print t("Here you'll find more information on the different commodities.") ?></p>
            </div>
          </div>
          <div id="drawer-front">
            <h3><?php print t('Kitchen drawer') ?></h3>
          </div>
        </div>
      </div>
      <div id="content">
        <?php print $content ?>
        <div class="block">
          <h2><?php print t('Search') ?></h2>
          <div class="content">
            <form action="<?php print url('cookbook/search/' . $url[0] . '/' . $url[2]) ?>" method="post" id="search">
              <div>
                <input type="text" id="search-field" name="keywords" value="<?php $url[1] == '0' ? print '' : print $url[1] ?>"/>
                <input type="submit" id="search-button" name="submit" value="<?php print t('Search') ?>" class="fading"/>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div id="footer">
        <div>
          <p>
            <?php print t('The Cookbook is developed by Amendor for NDLA') ?><br/>
            <a href="http://www.ndla.no" class="fading">www.ndla.no</a> | <a href="http://www.amendor.no" class="fading">www.amendor.no</a>
          </p>
        </div>
      </div>
    </div>
    <?php print $closure ?>
  </body>
</html>