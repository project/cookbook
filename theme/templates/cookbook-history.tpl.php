<?php
?>
<div class="block">
  <div class="content">
    <h3 id="recipe-header"><?php print check_plain($recipe->title) ?></h3>
  </div>
</div>
<div class="block">
  <h2 class="link">
    <a href="<?php print url('cookbook/recipe/' . $category . '/' . $search . '/' . $order . '/' . $page . '/' . $recipe->nid) ?>"><?php print t('Recipe') ?></a>
  </h2>
  <h2 class="short-margin"><?php print t('History') ?></h2>
  <?php if ($recipe->video != ''): ?>
    <h2 class="link short-margin">
      <a href="<?php print url('cookbook/video/' . $category . '/' . $search . '/' . $order . '/' . $page . '/' . $recipe->nid) ?>"><?php print t('Video') ?></a>
    </h2>
  <?php endif ?>
  <div class="content"><?php print check_markup($recipe->history, FILTER_FORMAT_DEFAULT, false) ?></div>
  <div class="tab-bottom">
    <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/' . $order . '/' . $page) ?>"><?php print t('Back to recipes') ?></a>
  </div>
</div>
