<?php
?>
<h3><?php print t('Steps') ?></h3>
<ol>
  <?php foreach (explode('@', $node->steps) as $step): if ($step != ''): ?>
    <li><?php print check_plain($step) ?></li>
  <?php endif; endforeach ?>
</ol>