<?php
?>
<h3><?php print t('Ingredients for 4 servings') ?></h3>
<ul>
  <?php if (variable_get('cookbook_use_ingredient_nodes', 0) == 1):
    foreach ($ingredients as $ingredient): ?>
      <li><?php print $ingredient->amount * 4 . ' ' . check_plain((is_null($ingredient->name) ? 'g' : $ingredient->name). ' ' . strtolower($ingredient->display)) ?></li>
    <?php endforeach;
  else:
    $ingredients = explode('@', $ingredients);
    foreach ($ingredients as $ingredient): ?>
      <li><?php print check_plain($ingredient) ?></li>
    <?php endforeach;
  endif ?>
</ul>