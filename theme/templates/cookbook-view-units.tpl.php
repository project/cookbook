<?php
?>
<h3><?php print t('Units of measurement') ?></h3>
<ul>
  <?php foreach ($units as $unit): ?>
    <li><?php print '1 ' . check_plain($unit->name) . ' = ' . $unit->grams . ' ' . t('grams') ?></li>
  <?php endforeach ?>
</ul>
