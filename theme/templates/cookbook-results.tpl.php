<?php
$path = base_path() . drupal_get_path('module', 'cookbook');
?>
<div class="block">
  <h2><?php print t('Categories') ?></h2>
  <div class="content" id="horizontal-categories">
    <table id="categories">
      <tr>
        <?php foreach ($categories[0] as $tid => $cat): ?>
          <td class="categories<?php if (is_array($categories['active']) && in_array($tid, $categories['active'])) print ' selected' ?>" id="category-<?php print $tid ?>">
            <a href="<?php $categories['active'][0] == $tid ? print url('cookbook/results/0/' . $search . '/' . $order . '/0') : print url('cookbook/results/' . $tid . '/' . $search . '/' . $order . '/0') ?>">
              <?php print check_plain($cat['name']) ?>
            </a>
          </td>
        <?php endforeach ?>
      </tr>
    </table>
  </div>
  <h2 id="tab-more" class="right"><span class="fading"><?php print t('More') ?></span></h2>
</div>
<div class="block" id="results">
  <ul id="sub-categories">
    <li class="show-all<?php if (count($categories['active']) < 2) print ' active' ?>">
      <a href="<?php print url('cookbook/results/' . $categories['active'][0] . '/' . $search . '/' . $order . '/0') ?>">
        <?php print t('Show all') ?>
      </a>
    </li>
    <?php foreach ($categories[1] as $tid => $cat): ?>
      <li class="sub-categories category-<?php print $cat['parent'] ?> <?php if (is_array($categories['active']) && in_array($cat['parent'], $categories['active'])) print ' show' ?><?php if (is_array($categories['active']) && in_array($tid, $categories['active'])) print ' active' ?>" id="category-<?php print $tid ?>">
        <a href="<?php print url('cookbook/results/' . $tid . '/' . $search . '/' . $order . '/0') ?>">
          <?php print check_plain($cat['name']) ?>
        </a>
      </li>
    <?php endforeach ?>
  </ul>
  <div class="content">
    <div <?php if($categories[1][$categories['active'][1]]['children'] == 0) print 'class="hidden" ' ?>id="sub-sub-categories-wrapper">
      <table <?php if (count($categories['active']) < 2) print 'class="hidden" ' ?>id="sub-sub-categories">
        <tr>
          <td class="show-all<?php if (count($categories['active']) == 2) print ' active' ?>">
            <a href="<?php print url('cookbook/results/' . $categories['active'][1] . '/' . $search . '/' . $order . '/0') ?>">
              <?php print t('Show all') ?>
            </a>
          </td>
          <?php foreach ($categories[2] as $tid => $cat): ?>
            <td class="sub-sub-categories category-<?php print $cat['parent'] ?> <?php if (is_array($categories['active']) && in_array($cat['parent'], $categories['active'])) print ' show' ?><?php if (is_array($categories['active']) && in_array($tid, $categories['active'])) print ' active' ?>" id="category-<?php print $tid ?>">
              <a href="<?php print url('cookbook/results/' . $tid . '/' . $search . '/' . $order . '/0') ?>">
                <?php check_plain(print $cat['name']) ?>
              </a>
            </td>
          <?php endforeach ?>
        </tr>
      </table>
    </div>
    <div <?php if($categories[2][$categories['active'][2]]['children'] == 0) print 'class="hidden" ' ?>id="sub-sub-sub-categories-wrapper">
      <table <?php if (count($categories['active']) < 3) print 'class="hidden" ' ?>id="sub-sub-sub-categories">
        <tr>
          <td class="show-all<?php if (count($categories['active']) == 3) print ' active' ?>">
            <a href="<?php print url('cookbook/results/' . $categories['active'][2] . '/' . $search . '/' . $order . '/0') ?>">
              <?php print t('Show all') ?>
            </a>
          </td>
          <?php foreach ($categories[3] as $tid => $cat): ?>
            <td class="sub-sub-sub-categories category-<?php print $cat['parent'] ?> <?php if (is_array($categories['active']) && in_array($cat['parent'], $categories['active'])) print ' show' ?><?php if (is_array($categories['active']) && in_array($tid, $categories['active'])) print ' active' ?>" id="category-<?php print $tid ?>">
              <a href="<?php print url('cookbook/results/' . $tid . '/' . $search . '/' . $order . '/0') ?>">
                <?php print check_plain($cat['name']) ?>
              </a>
            </td>
          <?php endforeach ?>
        </tr>
      </table>
    </div>
    <div id="results-content">
      <?php if ($search != '0'): ?>
        <div id="search-keywords">
          <?php print t("Search keywords") ?>:
          <span class="keywords"><?php print check_plain($search) ?></span>
          <span class="remove">
            <a href="<?php print url('cookbook/results/' . $category . '/0/' . $order . '/0') ?>" class="fading"><?php print t('Clear') ?></a>
          </span>
        </div>
      <?php endif ?>
      <div id="results-order">
        <?php print t('Order by') ?>:
        <ul>
          <li id="order-title"<?php $orders = cookbook_order($order, 'title'); print $orders[0] ?>>
            <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/title-' . $orders[1] . '/' . $page) ?>"><?php print t('Alphabetical') ?></a>
          </li>
          <li id="order-time"<?php $orders = cookbook_order($order, 'time'); print $orders[0] ?>>
            <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/time-' . $orders[1] . '/' . $page) ?>"><?php print t('Time consumption') ?></a>
          </li>
          <li id="order-difficulty"<?php $orders = cookbook_order($order, 'difficulty'); print $orders[0] ?>>
            <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/difficulty-' . $orders[1] . '/' . $page) ?>"><?php print t('Difficulty') ?></a>
          </li>
        </ul>
      </div>
      <div id="results-loading"><img src="<?php print $path ?>/theme/images/loading.gif" alt="Loading"/></div>
      <div id="results-recipes"><?php print theme('cookbook_results_xhr', $recipes, $hits, $category, $search, $order, $page) ?></div>
    </div>
  </div>
  <div class="tab-bottom">
    <span class="hits"><?php print $hits ?></span>
    <?php print t('hits') . ', ' . t('page') ?>
    <span class="pages">
      <?php $pages = $hits / 6; $page++;
      if ($pages - round($pages) > 0):
        $pages++;
      endif;
      $pages = round($pages);
      if ($page > 3): ?>
        <span class="page">
          <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/' . $order . '/0') ?>">1</a>
        </span>
      <?php endif;
      if ($page > 4): ?>
        <span>...</span>
      <?php endif;
      if ($page > 2): ?>
        <span class="page">
          <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/' . $order . '/' . ($page - 3)) ?>"><?php print ($page - 2) ?></a>
        </span>
      <?php endif;
      if ($page > 1): ?>
        <span class="page">
          <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/' . $order . '/' . ($page - 2)) ?>"><?php print ($page - 1) ?></a>
        </span>
      <?php endif ?>
      <span class="page selected"><?php print $page ?></span>
      <?php if ($page < $pages - 1): ?>
        <span class="page">
          <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/' . $order . '/' . $page) ?>"><?php print ($page + 1) ?></a>
        </span>
      <?php endif;
      if ($page < $pages - 2): ?>
        <span class="page">
          <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/' . $order . '/' . ($page + 1)) ?>"><?php print ($page + 2) ?></a>
        </span>
      <?php endif;
      if ($page < $pages - 3): ?>
        <span>...</span>
      <?php endif;
      if ($page < $pages): ?>
        <span class="page">
          <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/' . $order . '/' . ($pages - 1)) ?>"><?php print $pages ?></a>
        </span>
      <?php endif ?>
    </span>
  </div>
</div>