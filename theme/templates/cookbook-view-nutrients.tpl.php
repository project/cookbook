<?php
?>
<h3><?php print t('Nutrients') ?></h3>
<dl>
  <?php foreach (cookbook_get_nutrients (FALSE) as $key => $values):
    if ($node->$key != -1): ?>
    <dt><?php print $values[0] ?></dt>
    <dd><?php print $node->$key . ' ' . $values[1] ?></dd>
    <?php endif;
  endforeach ?>
</dl>